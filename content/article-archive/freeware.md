---
title: "Freeware"
date: "2024-07-15T23:04:44-04:00"
authorbox: false
tags:
  - "freeware"
  - "Computer Magazine Article"
  - "Today magazine"
  - "software distribution"
---


An Optimistic Approach to Software Piracy

By Charles Bowen and J. Stewart Schneider

from the January/February 1983 issue of TODAY magazine

Fellow man.

It's the kind of faith that, if contagious, could spawn a whole new kind of marketing in the microcomputer community.

At a time when major software houses are spending tens of thousands of dollars in what some say is a futile effort to protect their programs against pirates, a man named Andrew Fluegelman gives his programs away.

It's called "Freeware," and to those who've been burned before by the deals of software hustlers, it sounds disarmingly straight forward:

You send Fluegelman a blank disk and postage (but no money) and, by return mail, he sends you a copy of his program. You try it out, and if you like it and decide to use it, you are invited to send him a "contribution" — he suggests $25. Or you could send nothing. The choice is yours. Further, rather than discouraging piracy, he urges you to make copies for your friends, and he offers them the same terms — pay him if they like the program.

When the 39-year-old Tiburon, Calif., man introduced the Freeware concept on the bulletin boards of CompuServe and The Source last April, veterans of the software piracy wars called Fluegelman a cockeyed optimist, doomed for disillusionment. After all, he was a rank neophyte in the software marketplace — a year earlier, he had never even touched a personal computer.

But Fluegelman was no stranger to alternative forms of publishing, and these days many people are not so quick to dismiss Freeware as just so much blue-skying.

Six months after announcing Freeware's first program — a communications package for the new IBM personal computer called "PC TALK" — Fluegelman says he had distributed more than 1,500 copies and collected some $15,000 in contributions. He also made some pretty good friends.

In fact, the feedback from recipients of PC TALK has convinced him that Freeware is not just a novel way to distribute one program. Fluegelman is convinced that "user-supported" software is at least a new marketing concept, and just may be the boon creative programmers are looking for.

"The best part of that feedback is that is never comes in the form of complaints or gripes," Fluegelman says. "Users appreciate the spirit in which the software is being offered and they offer their comments in the same spirit."

Much of the feedback has been suggestions from users on how to improve the program, resulting in two upgrades already.

"I received many good suggestions from users about how to deal with their special hardware/software needs," he said. "I couldn't have anticipated all of them. You asked how I managed to educate myself so quickly in such a complex field as computer communications. I'd definitely have to credit and thank my 'subscribers' for providing so much good information."

## Birth of a Concept

In a sense, the idea of Freeware was eight years in the making, growing out of the several careers Andrew Fluegelman has had in the past 15 years.

After graduating from Yale Law School in 1968, he practiced corporate law in his native New York City and then in San Francisco. But after five years, he became "a 'victim' of the late '60s" and discontinued his law practice. He wound up as managing editor of The Whole Earth Catalog for about two years.

In the late 1970s, his interest in publishing heightened by his experiences with the Whole Earth products, Fluegelman struck out on his own and established an independent book producing firm. The Headlands Press since has created books independently and sold them to national publishers for distribution, "much the way that independent film and record producers work," he says.

Headlands' credits range from two game books for Doubleday to "A Traveler's Guide to El Dorado and the Inca Empire" for Penguin to the upcoming "Grateful Dead: the Official Book of the Deadheads," for Morrow — "an eclectic list," he admits, but one that "has happily allowed me to pursue interesting subjects."

While pursuing just such a subject, Fluegelman came upon microcomputers, a new editorial assignment — and yet another career.

"Last year, I was looking for a new book project and decided to do a book about word processing. (The result: "Writing in the Computer Age," written with Jeremy Joan Hewes, to be published this February by Doubledy.)

"I just happened to be shopping for a computer when IBM announced its Personal Computer and I wound up becoming one of the first owners of the PC. I immediately became fascinated and addicted to the machine."

Fluegelman's joy with microcomputing soon had him writing reviews of software and eventually led to his accepting a position as associte editor of the new PC Magazine for IBM users.

Fluegelman's journey into the software business came a few months later.

"As Jeremy and I were starting to work on our book, it became necessary for us to send drafts back and forth between our computers," he says. "IBM had released its own asynchronous communications software, but it was very limited. Aside from what has been generally acknowledged to be a 'clunky' program design, the software could only communicate with other PCs at the micro level. Since Jeremy was working on North Star running CP/M, I had to figure out how to communicate with her, so I started writing my own communications program."

That BASIC program was the beginnings of PC TALK. It was originally intended only for Fluegelman's personal use, but a few friends started using it and convinced him that he should try to market it.

So far, the story of PC TALK is fairly common among a new breed of software authors who turn their personal programs into marketable products.

"I think that if I had not just spent the previous eight years dealing with traditional marketing approaches — publishing, royalties, advertising, distributors, etc. — I would have jumped ahead and gone that route," he says.

"But I felt I had just immersed myself in a new field — had created something in a new medium — and somehow the thought of treading those same, familiar selling paths didn't seem very exciting."

During those days of the final polishing stages for PC TALK, Fluegelman was watching the local public TV station, KQED, holding its annual pledge drive. "The idea for 'user-supported software' popped into mind."

## "Network nation"

The idea born casually during a moment of relaxation has since grown into three basic principles of the Freeware that Fluegelman has written into a policy statement:

— First, that the value and utility of software is best assessed by the user on his/her own system. Only after using a program can one really determine whether it serves personal applications, needs and tastes.

— Second, that the creation of independent personal computer software can and should be supported by the computing community.

— And, finally, that copying and networking programs should be encouraged, rather than restricted. The ease with which software can be distributed outside traditional commercial channels reflects the strength, rather than the weakness, of electronic information.

Fluegelman chose to spread the word about his new program solely on the computer bulletin boards of CompuServe and The Source and on some smaller systems such as the Capitol PC Users' Group in Washington, D.C. He has never advertised in a print publication, and, looking back on it, he realizes the "network nation" was instrumental in the philosophy of Freeware.

"There is a special quality about computer networking that makes Freeware work," he says. "After all, the Freeware concept came about precisely because of the free exchange of information that the computer age offers."

## Legalities

Of course, any new concept is bound to have some new legal questions attached to it. In Freeware's case, can you copyright a program you "give" away? Do you have any legal recourse if someone "steals" it and starts marketing it as his own?

Fluegelman says yes to both questions. PC TALK carries a copyright notice in its title page, along with a "limited license" to the recipient to use and copy the program and a request for a contribution. Fluegelman admits that the validity of the limited license may be questionable under current court precedent, "but, of course, the law responds to new economic and moral situations. I don't think it's possible to offer an iron-clad legal opinion about what Freeware really is and how and to what extent it can be protected. "

He says he believes if some one else tried to market PC TALK, he'd have a pretty good shot at going into court and making him stop and/or pay damages. However, he insists the plan is not based on a strong legal protection. Instead, it depends on people's better nature.

## A touch of guilt

And something else — just a touch of guilt. Each time you run PC TALK, the first thing you see is a notice reminding you that the author is asking for a $25 contribution if you like the program. Fluegelman says that these two motivations — the better nature and the guilt factor — working in concert are what make Freeware successful.

"If someone finds the program useful and adopts it as their regular, day-to-day communications program, the Freeware notice will remind them that they have received something of value. How 'guilty' they feel is up to them. But that situation is no different than someone making a copy of a commercial program and using it without having paid for it.

"The one wrinkle with Freeware is that the program became available to the user with no strings attached. The implied offer is to try the program first and pay only if the user finds it worthwhile.

"And that's where 'guilt' starts becoming transformed into 'good faith.'"

Fluegelman notes that almost every contribution he's received has been accompanied by a personal note saying how much the user liked the program and how pleased he is to be making the contribution.

So, could someone make a living distributing user-supported programs?

Freeware has not allowed Andrew Fluegelman to just sit back and watch the money roll in. Book publishing and editing is still his primary source of income. And, he's had to hire a full-time assistant to help with the paperwork involved with the software business. Requests for PC TALK have increased from 1 5 to 25 a week back in June to about 15 to 20 a day in October. And Fluegelman still keeps records of all the people who requested the program — "the Freeware network" — so he can notify all the contributors of upgrades to the program.

"There's no question that Freeware has not been losing money," he says, "but, frankly, I'm not sure how well it would support someone over the long haul."

On one hand, Freeware saves money on what are generally "high-ticket items" in traditional software distribution — advertising, software protection and materials (disks and tapes).

## No goldmine

From a purely economic viewpoint, one of Freeware's major problems is the dropoff in contributions from the "secondary users" — those who accept Fluegelman's invitation to get a copy of the program from a friend under the same "limited license." Fluegelman says he eventually receives contributions from about two-thirds of those who send him a disk and request a copy of PC TALK. However, he estimates only about 15 percent of the second-generation users become contributors. (The reason may be psychological, he notes. The secondary users didn't initially "invest" a disk in the experiment, and without that investment, there's less incentive to pay.)

"So Freeware hasn't turned into a 'circle of gold' just yet," Fluegelman says, "but I can't get upset or be too concerned with that part of it. After all, the whole point is not to put any energy into worrying about 'unauthorized' copies."

The Freeware experiment is successful enough that Fluegelman says he plans to continue it this year on several fronts:

First, a new version of PC TALK (Version 3.0) will be available by the time this article is published. He plans to send all prior contributors a voucher for credit to update for the cost of mailing and a disk.

Secondly, Fluegelman is looking for other programs to distribute under the Freeware banner. He hopes eventually to publish a catalog of user-supported programs available from their authors. For the purposes of the experiment, he would like to see a user-supported game program. He realizes that PC TALK owes part of its success to the fact that it was a "serious" utility program for a relatively new computer. Had the first Freeware offering been a game program for a machine like Apple, the percentage of contributions might have been lower, but on the other hand, the overall distribution might have been larger.

If the Freeware experiment catches on, it could play an important part in the state of the art of microcomputer programming, Fluegelman believes.

"Up to now, distribution of software has relied either on restricting access (and charging for the cost of doing so), or anonymously casting programs into the public domain. The user-supported concept might — just might — be a way for the computing community to support and encourage creative work outside the traditional marketplace.

"If one is going to distribute a program for 'free' and hope for voluntary contributions from users, the program has got to provide the same level of friendliness and support that one would expect of a 'commercial' product."

Ultimately, Freeware is an experiment in economics more than altruism, its creator says.

"Free distribution of software and voluntary payment for its use would eliminate the need for money to be spent on marketing, advertising, and copy protection schemes. Users could obtain quality software at reduced cost, while still supporting the program authors."

"And the most useful programs would survive, based purely on their usefulness."

Anyone wishing to communicate with Andrew Fluegelman can reach him c/o The Headlands Press, Inc., P.O. 862, Tiburon, CA 94920. His CompuServe ID # is 71435,1235.

*Charles Bowen is a journalist with 16 years experience in the newspaper business, including eight years as city editor of the Huntington, (W.Va.) Herald-Dispatch. J. Stewart Schneider is an attorney in Ashland, Ky. Bowen and Schneider manage Saturday Software, which is dedicated to writing programs for home computers and articles about the computing community.*
