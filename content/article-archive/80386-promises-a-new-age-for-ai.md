---
title: "80386 Promises a New Age for AI"
authorbox: false
tags:
  - "AI"
  - "Computer Magazine Article"
  - "Computer Language"
  - "30386"
---
By Susan J. Shepard

from the July 1987 issue of Computer Language

The Intel 80386 microprocessor is here—at last. It is proving to be all that we expected, and the last obstacle to 386 supermicrocomputers is an operating system that can avail the developer and user of its power. It will provide a powerful and affordable platform for expert systems and other AI applications that are truly useful.

AI has been been waiting for this chip; many AI applications developers whispering, “When the 386 is available...” and hinting of powerful systems for the desktop and a rich development environment for the programmer working and learning in the real world.

The Intelligent Computer Systems Research Institute at the University of Miami, Coral Gables, Fla., predicts a threefold increase in expert systems this year, largely because the 80386 has arrived. In fact, the processor narrows the gap between micros and LISP workstations significantly—at a substantial dollar difference.

What will micro-based 386 systems offer when an adequate operating system is available for the industry-standard MS-DOS compatibles? What do available 386 systems offer for AI development? Pending a widely available operating system with downward compatibility to prior MS-DOS software, AI tools implemented for that environment will have to run on 386 machines within the constraints of DOS and 16 bits. Is it worth moving up to 386 running under DOS now?

Each of these questions is complex, and many factors must be weighed to ensure that time and money are invested wisely now and prior hardware and software investments are protected. We can start by looking at the 80386 itself and then consider the Inboard 386/AT system, selected for best price, best performance, and compatibility, to see how it handles some basic AI tools for micro development.

We'll also look briefly at Gold Hill’s 386-based LISP development system built to deliver an optimized hardware and software environment to existing IBM XT/AT compatibles.

## Intel’s 80386

Intel’s 80386 is a high-performance, 32-bit microprocessor designed for computer-intensive applications such as CAD/CAM, high-resolution graphics, high-level factory and office automation, and publishing. More importantly, it will permit advanced systems generally termed AI to move toward production applications levels. Machine vision, speech recognition systems, and more complex expert systems will become more feasible and affordable. The 32-bit 386 integers and 80-bit 80287/80387 floating point numbers can easily support the speed and accuracy of computation required for fine precision and axis control of an industrial robot.

Frequencies of 12 MHz and 16 MHz are now available, and a 20-25 MHz frequency is forthcoming. The 16 MHz, running without wait states, can achieve a sustained 3 to 4 millions of instructions per second (MIPS). In addition to faster clock speeds, the 16 MHz version has lower clock counts.

This architecture has the programming resources required to directly support applications characterized by large integers, large data structures, and large programs or large numbers of programs. Physical address Space is 4 gigabytes; logical address space is 64 terabytes. Eight 32-bit general registers can be used interchangeably as instruction operands and addressing mode variables. It has a complete instruction set for manipulating data types and controlling execution. Data types include 8-, 16-, and 32-bit integers and ordinals, packed and unpacked decimals, pointers, and strings of bits, bytes, words, and double words.

At this speed, memory access is obviously a potentially serious problem. The 80386 bus is designed to use both 45 nanosecond static RAM (SRAM) and slower, less expensive 100ns-120ns dynamic RAM (DRAM) chips. Zero wait state caches are supported, and their size can be anywhere from the useful minimum 4K to the entire physical address space.

Other features are virtual memory support, configurable protection, new debug registers, and object code compatibility. The 80386 has four modes: real, virtual 8086, protected, and native 80386. Real mode is entered after reset; it is essentially a very fast 8086 mode. Virtual 8086 uses special flags, registers, and interrupts to permit fast, safe multitasking in environments that extend the concept of virtual memory in individual programs to include an entire PC.

Protected mode emulates the 80286; at this time neither the 80286 nor 80386 using MS-DOS can take advantage of the protected mode, with the exception of Specialized software such as Gold Hill’s GCLISP 286 and 386 Developer packages or Texas Instrument’s PC Scheme and Personal Consultant PLUS. Native 80386 permits operating systems and programs up to 4 gigabytes of RAM and can address 64 terabytes of virtual memory.

Virtual memory enables the maximum size of a program or programs to be determined by available disk space rather than RAM. AI applications are typically memory intensive, and virtual memory means more flexible delivery of higher performance software on less expensive hardware. Programmers can leave storage management to the operating system rather than writing overlays, to an extent that is not yet clear.

For example, how much real memory is needed to support a certain amount of virtual memory? B.E. Smith and M.T. Johnson, in Programming the Intel 80386, estimaté that about twice as much virtual memory as real memory can be supported.

The 80386 provides a range of protection facilities that can be applied selectively as the application warrants: separation of task address spaces, from 0 to 4 privilege levels, typed segments, access rights for segments and pages, and segment limit checking. Protection checks are performed in the on-chip pipeline to maximize performance. These facilities are particularly useful when developing programs the size the 80386 is built to handle—hundreds or thousands of modules.

Four on-chip debug registers enable code or data break points. The registers are independent of the protection system and can be used by all applications. Conventional debugging through a break point instruction and single stepping: are supported as well.

Object-code-level compatibility with both the 80286 and 8086/88 processors protects existing software investments and allows rapid development for the 80386. 80286 and 80386 programs can run concurrently. The 80286’s architecture is a proper subset of the 80386’s, so existing 80286 operating systems and applications can be ported without change. Virtual 86 is a compatibility feature that establishes a protected 8086/88 environment within the 80386’s protected multitasking environment. 80386 paging can give each Virtual 86 task a 1 MB address space anywhere in the physical address space and an 8086 environment. It is possible to build systems that could execute 80386, 80286, and 8086/88 software concurrently. 8086 software can run in the real mode of the 80386, but timing-dependent programs may need modification to function properly in the much faster mode.

![Table 1](/img/30386-table1.jpg)


![Table 2](/img/30386-table2.jpg)

## Hands on

The Intel Inboard 386/AT add-in 80386 system with 80287 or 80387 for the IBM AT and close compatibles offers a reasonably priced, high-performance entry to 80386-based micros. We will look closely at that product, and run some AI applications on it (Tables 1-4), as a way of indicating the tremendous improvement in an AI development project using the 80386.

Other products giving good performance are, of course, the Compaq 386 series of personal computers and the Gold Hill 386 LISP System. The latter is an add-in board for the IBM PC XT or AT with a sophisticated LISP development software package.

Because these systems are based on the 80386, my earlier discussion applies to each of them, with the exception of SRAM caching and 80387 support. Cache implementations are very important for achieving good performance, and different approaches should be evaluated before purchase. Vendors should be well-queried about performance and compatibility for your needs.

The marketplace for 80386-based AI is changing rapidly. Joining Gold Hill in the race to exploit the chip’s power— with or without an operating system— other major vendors are making announcements. Lucid Corp., which offers AI tools for workstations, IBM PC RT, and other large systems, is developing a package that will run under UNIX on the 80386. Because UNIX is widely used on workstations, ports will increase for both the operating system and product.

My evaluation of the options led me to choose the Inboard as the best solution for my needs, offering compatibility both with present software and future expansions. Its performance for the languages and tools I use, and its overall system improvement, has exceeded Intel’s claims for the board. I found that I not only realized a two- to three-fold increase in execution times for software at 16 MHz, but I also got better performance indexes from a rather old Seagate ST-225 hard disk using the CORE disk performance test v. 2.7. This was a pleasant surprise, but replacing the disk with a faster unit would improve performance tremendously.

![Table 3](/img/30386-table3.jpg)

![Table 4](/img/30386-table4.jpg)

Specifications for the board are: speed selection ranging from 16 MHz to the original system speed; 256K x 32-bits memory size; 120ns DRAMs up to 3 MB of 32-bit memory and access to other extended or expanded memory in the host system; and a direct-mapped 45 nanosecond SRAM cache, 64K x 32 bits. Intel claims that 90% of the 80386’s memory requests are filled from this 64K cache instead of the older, slower system memory.

Overall performance indicates that hits are at least 90% when memory outside the zero wait-state cache must be accessed and wait states are introduced. However, since these are lower clock count 80386 waits, the effect is not as severe as on the 80286.

The board uses a 16-bit slot in the host machine. The 80286 chip is replaced by an ingenious cabling technique from the system board CPU socket to the 80386 board: Control software bridges differences in 80386 and 80286 error handling and offers other easy-to-manage drivers. Speed is software-switched.

The move from 8 MHz to 16 MHz has caused no compatibility problems with any of the current AI language implementations or expert system tools for MS-DOS, with the exception of the Gold Hill GCLISP 286 Developer. This product will not run on the Inboard, but the 386 Developer will run on the Compaq 386 and Gold Hill’s Hummingboard.

Compatibility with the Inboard could not be tested. Because the performance of these individual applications varies widely, the impact of the 80386 can best be described as a big improvement. Waits for notoriously processor-intensive LISP interpreters are reduced anywhere from 25% to 75%. Shells improve in a similar fashion.

The benchmarks for extended memory PC Scheme (Table 1) make that package’s 2 MB memory capability attractive. Gold Hill’s GCLISP interpreter (Table 2) improves significantly as well.

The improvement in 80286 or 8086/88 AI tools is significant without even attempting to optimize code for the environment. The 80386 architecture offers a great opportunity for AI. Very soon, AI tools that can exploit this architecture will join Gold Hill’s package, and applied AI may finally emerge as a major element in practical computing.

## References

Abelson, H., and G.J. Sussman. Structure and Interpretation of Computer Programs. The MIT Press, 1985.

Gabriel, R.P. Performance and Evaluation of LISP Systems. The MIT Press, 1985.

Introduction to the 80386. Intel Corp., 1986.

Smith, B.E., and M.T. Johnson. Programming the Intel 80386. Scott, Foresman and Co., 1987.

-----

Susan Shepard is a writer and editor specializing in the microcomputer field. She is the AI Insider columnist for AI EXPERT. In addition to doing private consulting, she is a program consultant for the Academy for Educational Development, an international consulting firm based in New York, N.Y.
