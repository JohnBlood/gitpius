---
title: "The Business Evolution of the Unix System: An account from the inside"
date: "2024-08-04T22:34:22-04:00"
authorbox: false
tags:
  - "Unix Review"
  - "UNIX"
  - "Computer Magazine Article"
---

by Otis Wilson

from the January 1985 issue of Unix Review magazine

![cover](/img/business-evolution-unix.png)

Thanks to the developers of the UNIX operating system, and to the research method at AT&T Bell Laboratories, the technical evolution of the UNIX System has been well documented and its history largely understood. From a technical perspective, there just isn’t much argument about who did what when and why things were done the way they were.

On the other hand, the "business” history of the UNIX system is largely an oral one, rich in folklore and popularized by the modem press in hopes of finding some explanation for the phenomenon that is the UNIX system.

For all its entertaining quality, the oral tradition is not very instructive, especially for those, including AT&T, that will build their futures on the success of UNIX software.

Although a complete business history is beyond the scope of a single article, a few insights into the major events can put the “evolution” into its proper perspective, and can give us all a better view of what the future might hold.

## INSIDE THE CORPORATE CULTURE

Nothing has had a more profound impact on the business development of the UNIX system than AT&T’s own corporate culture. And although there have been many “cultural” influences that have molded the evolution, two stand out most prominently.

The first is the well-established intellectual bond between AT&T Bell Laboratories and the academic community — one that goes beyond formal internship and summer on-campus programs. The relationship has been nurtured throughout the history of Bell Labs, and it has contributed significantly to the Labs’ effectiveness as one of the world’s leading research institutions. It has also had more than a casual impact on AT&T’s ability to attract outstanding technical talent.

The second major influence was the 1956 Consent Decree, the settlement of a complex antitrust case that would, among other things, clearly define the Bell System’s business. In its definition, the Consent Decree was unequivocal: the Bell System was to provide telecommunications products to the Bell Telephone Companies and telecommunications service to the nation — and nothing more.

## THE CONSENT DECREE AND SOFTWARE

Under the terms of the Consent Decree, the Bell System was required to license certain technologies that had been funded by its regulated business, and an organization. Patent Licensing, had to be set up to manage the effort.

But contrary to popular belief, the Consent Decree did not address the issue of software. In fact, the technology was virtually unknown at the time. Consequently, AT&T was not specifically obligated to license its software; nor was it restrained from entering the software business. Software was a muddy issue in an otherwise clear statement of AT&T’s legal responsibilities.

As early as 1967, even before the development of the UNIX system, the technical relationship between Bell Laboratories and universities across the nation had spawned a number of requests for AT&T software. In the interest of further cultivating that relationship, there was a genuine desire to share the technology. There were also concerns. Was the company required to, or enjoined from, licensing software? A definitive answer would have required revisiting the Justice Department’s ruling, and, understandably, few in the company were inclined to raise any issue that might require doing so.

Instead, the company took a more prudent approach. To preclude any conflict with the Consent Decree, AT&T would license its software under the Consent Decree’s legally established procedures but would made it clear that it had no intention of pursuing software as a business. The policy was restated over and over again at every gathering of the faithful — “As is, no support, payment in advance!”

## ENTER UNIX SOFTWARE

It was in this climate of technical exchange and legal uncertainty that the UNIX system first spread to academia in the early 1970s. At the time, the UNIX system was still a research project, and its impact on AT&T’s total computing environment was minimal. In fact, it’s fair to say that many colleges and universities knew more about the UNIX system than did AT&T’s mainstream data processing community.

By 1972, educational interest in the UNIX system had grown to the point that there were a handful of universities requesting copies of the software for use in their computer science programs. Initially, requests were referred to the the developers themselves, and through the patent attorneys at Bell Laboratories, the software was conveyed royalty-free under simple letter agreements.

Within a few months however, as the volume began to tax the resources of the Labs, responsibility for licensing fell to AT&T’s Patent Licensing organization, the group that had been vested with the responsibility for licensing intellectual properties under the Consent Decree.

## INSIDE PATENT LICENSING

Patent Licensing was an organization with an intentionally low profile. Historically, negotiations for AT&T patents were conducted at very high levels and the discussions were particularly sensitive. The organization’s management would typically be involved in only four or five negotiations per year, but each might stretch on for months and involve teams of lawyers, accountants, and millions *bf dollars. It was, by any measure, a high stakes game.

In such an environment, non-revenue-producing software was a low priority. The lack of commitment was no more evident than in the licensing organization itself.

There were those, however, who recognized the potential for software. By the early 1970s, there were approximately two dozen software packages that had been released by AT&T under license agreements. Of them, 10 showed significant potential. The UNIX system was the clear leader.

Those 10 packages formed the basis of a “mini” business case, but few would be convinced of the need for additional resources to support the licensing effort. AT&T’s policy was clear, and it would withstand a number of challenges before being reversed.

## ENTER COMMERCIAL INTERESTS

Had the licensing of UNIX software been restricted to universities, the story might have ended there. But the emergence of commercial interest in the software placed new demands on the organization and forced a decision that would eventually lead to the creation of a market.

By 1974, AT&T had established nominal distribution fees for the software, but the income from universities was insufficient to offset the costs associated with licensing. When the company actually received its first commercial requests in 1974, there were no commercial prices in place, nor was there much of a notion about how the software should be priced for commercial use.

Although most agreed that commercial licensing would help the software effort earn its keep, there was little initial agreement on how the pricing should be structured. Many felt the price should be set abnormally high to discourage widespread use. Under such a scheme, income from just a few licenses would provide more than enough income to defray licensing expenses. Others, including the developers, felt the price should be set artificially low to encourage use and experimentation. Still others argued for a price that would be competitive with existing systems (minus an adjustment to reflect the lack of support). Significantly, the company chose to base the software price on market considerations.

The decision would have a profound influence on the business history of the UNIX system. It indicated, for the first time, the company’s genuine business interest in the software. More importantly, it led to the establishment of a “customer base” whose needs were radically different from those for whom the software had been designed.

Clearly, commercial users of the system considered themselves “customers” despite AT&T’s disclaimer that it had but one customer — namely, the Bell System. Commercial users wanted greater functionality, more commercial features, more flexible terms and conditions, as well as additional rights, including the right to sublicense. They were the impetus for change, and a constant challenge to AT&T’s established policy. The changes were slow in coming, a source of frustration to many of them and a condition that set the stage for the love/hate relationship that has long characterized UNIX software in the commercial world.

## A BUSINESS EMERGES

During the period 1974-1978, AT&T introduced several versions of the UNIX system for a wide variety of DEC hardware, and each release prompted an increase in the number of requests for the software.

For the most part, licensing policies and pricing remained consistent, but functional differences increased both internal and external pressures to provide a single, consistent and — from an internal viewpoint — supportable version that could be used in a production environment. There were also demands from external licensees to incorporate more commercial features that had been developed elsewhere.

In late 1980, Western Electric, the manufacturing and supply arm of the Bell System, essentially “bought out” its other corporate partners that provided the funding for UNIX system development and established a single funding source. Responsibility for production and distribution of the source tapes was moved from Bell Laboratories, which had performed those functions from the beginning, to Western Electric. Although a firm business commitment was not yet established, it was clear that the company was beginning to think about the UNIX system as a product. After all, that was Western Electric’s role in the Bell System.

Under Western Electric’s direction and in response to AT&T’s largest external customer, the federal government, a number of efforts were made to provide a more “commercial” product. Research notes were assembled to form official “documentation”, and a support organization was established to assist government licensees. What emerged from the whole of this effort was UNIX System III.

UNIX System III was enormously successful in many respects. For the first time, it combined in a single release many of the best features of the earlier versions, plus some enhancements of its own. It was also the first version to be released externally at about the same time it was released internally. Historically, “commercial” releases lagged significantly behind releases to the Bell System. But most significantly, its success demonstrated, without a doubt, the market potential of UNIX software.

The success of System III also brought its share of problems, not the least of which was the extra strain it put on the resources of Patent Licensing. The licensing process was still a legal one that required, in the opinion of many AT&T customers and AT&T officials, an extraordinary length of time. Licensing delays were creating a lengthy backlog of unfulfilled requests. Delay had always been a hallmark of UNIX software licensing, but considering the resources devoted to the effort, it had always been manageable. The licensing of System III rapidly became unmanageable.

On the software front. System III represented a good first step in the right direction, but there was still much to be done before the software could be considered a product. For all its merits, System III still lacked many commercial features that licensees were demanding.

In the meantime, renewed antitrust action signaled a fundamental shift in AT&T’s thinking about its traditional business role. The time had come to seriously evaluate the company’s potential as a software vendor. In January, 1983, just a few weeks after the announcement of the proposed Modified Final Judgement that would bring about the dissolution of the Bell System, AT&T formally announced its intentions in the software business by releasing UNIX System V, complete with training and technical support. It also pledged its commitment to UNIX System V as the foundation of AT&T’s future development efforts.

To achieve that charter, AT&T set up Software Systems as a “sub-business” under the Computer Systems Division. The first organization under that new structure was the Software Sales and Marketing organization, whose responsibility it would be to make the transition from a licensing organization to a customer-oriented sales organization. The licensing of AT&T’s other software, which had, by necessity, taken a back seat to UNIX software, remained with the Patent Licensing organization.

The first item on the agenda of the new organization was to reduce the enormous backlog of pending requests. Within six
months, the backlog had been reduced from more than 200 to less than a handful by carefully restructuring the effort to scale down the need for extensive legal review, by cutting out unnecessary paperwork, and by instituting standard agreements that could be easily adapted for any UNIX software product. As a result, average turnaround time for processing agreements was reduced from four weeks to one.

A second major objective was to rebuild relationships with those licensees who offered products based on AT&T software. Towards that end, a new, more flexible royalty schedule was offered to more adequately reflect the reality of the microcomputer marketplace. An on-going communications program was also established and a visitation program was implemented.

In addition, a variety of add-on products are currently being offered, and many of the commercial features so long requested by commercial resellers are being implemented. Perhaps most importantly, the future direction of AT&T’s development effort has been documented, providing a blueprint for those who will follow AT&T’s lead with UNIX System V.

There is still too much to be done to close the book on the UNIX system’s business history. But much of the remaining story will be written under the influence of a new corporate culture — one characterized by a business commitment to the company’s custodial role. There will also be a number of contributing authors — other vendors, who like AT&T, have grown to recognize the potential of the UNIX system.

---

_Otis Wilson is the manager of AT&T Technology System's software sales and marketing efforts. His contributions were key to the design of AT&T's current licensing structure and his leadership is still central to its implementation. Mr. Wilson is also a member of the UNIX REVIEW Editorial Board._
