---
title: "Pizza Parlor Computing"
authorbox: false
date: "2024-07-12T01:31:36-04:00"
toc: false
tags:
  - "pizza"
  - "Computer Magazine Article"
  - "Today magazine"
  - "Nolan Bushnell"
---

![header](/img/pizza-parlor-computing1.png)

by Francine Sevel

from the July 1983 issue of TODAY magazine

Just the right touch of entertainment is often as much a part of a restaurant's charm as that secret recipe handed down from generation to generation. And, as time and technology have revolutionized every aspect of society, restaurants have had to keep pace. Even pizza parlors have not escaped the wheels of motion.

Today's number one pizza chain not only has a full array of pizza selections: double cheese, thick vs. thin crust, anchovies vs. olives, salad bar, deli sandwiches, beer and wine, but also the state-of-the-art equipment for high-technology entertainment.

Enter the era of megatrends. Replace the juke box with eight-foot-tall singing and dancing robotic characters. Replace the pool table with 40 or so video games, add small rides for the tykes, a large-screen television, and, last but not least, add a handful of microcomputers, and some plans to teach computer literacy to America.

Call your pizza chain ShowBiz Pizza Place or Chuck E. Cheese's Pizza Time Theatre Inc. And make sure that either Nolan Bushnell or the Brock Hotel chain is standing behind you, because you're going to need big bucks.

Those in the business refer to restaurants such as Chuck E. Cheese and ShowBiz as family entertainment centers. "We're really in the entertainment business as much as we're in the food business," states Sam Polack, senior executive vice president and chief operating officer of ShowBiz. "The very fact that entertainment is constantly changing dictates that we change with it . . . stay with or ahead of what the public wants." Thus the logistics behind both chains' recent addition of microcomputers to their menus.

If the old adage — you can tell the men from the boys by the price of their toys — is true, then the family entertainment center is definitely a rich man's sport. At ShowBiz, local talent includes "The Rock A-Fire Explosion," a group of robotic animal characters who sing, play instruments, and perform comedy routines. The act is hosted by the infamous Billy Bob, an amiable hillbilly bear, and the Birthday Bird, Billy Bob's happy "bird-day" buddy.

Similarly, at Chuck E. Cheese's, the floor show is provided by a robotic rat named, you guessed it, Chuck E. Cheese, and an eight-foot robotic lion who imitates another king — Elvis Presley. The duo is accompanied by other singing and dancing robots such as Madame Oink, a French pig, and Pasqually, a concertina-playing pizza chef.

Pizza Time's entertainment is the result of a 2.5-million-dollar development effort centered on the extensive use of computer animation. A DEC PDP 11/23 is used to program the characters' movements. Every minute of recorded skit is the direct result of five hours of programming efforts.

In the entertainment industry, change is the only certainty. In order to stay ahead of the competition, it is necessary to replace five or six video games every six months. Plan on spending at least $2,500 per video game. "It's a very capital intensive business," comments ShowBiz's Polack. "It takes a lot of money to keep these games fresh and putting in new ideas. Many of the competitors aren't willing to do that ... if they don't or we don't, then the concept is difficult to keep in motion." Though software is less expensive to replace than computer games, ShowBiz's specially designed computer units, which house Apple II computers in a colorful shell, carry an initial investment of $4,000 per computer.

But you know what they say: You have to spend money to make money. Currently, ShowBiz Pizza sells more pizza per store than any other chain in the country. Comprised of 140 company-owned entertainment centers, plus franchises, ShowBiz is a subsidiary of the Brock Hotel chain, which, incidentally, is also the nation's largest franchiser of Holiday Inns.

The competition isn't exactly hurting either. A chain of over 214 units, Pizza Time Theatre Inc., grossed over $200-million in 1982. And it has projected that number of units will topple the 300 mark by the close of 1983.

Pizza Time Theatre, Inc. is the brainchild of Nolan Bushnell. Nicknamed the Pied Piper of Sunnyvale by *Inc.* magazine, Bushnell is the father of Pong, the pioneer of computer games, and Atari, Inc. , an 11 -year-old company which gobbles up approximately two-fifths of today's video game industry. A comtemporary King Midas, Bushnell sold Atari to Warner Communications, Inc., in 1976 for $28-million. When it appeared that Warner wasn't interested in making pizzas, Bushnell and Joseph Keenan (now president of Pizza Time) bought it back for $400,000. Today, at 38, Bushnell sits as board chairman of Pizza Time Theatre, Inc.

![header](/img/pizza-parlor-computing2.png)

Though game revenues represent only about 25 percent of both chains' total sales revenues, they are a key ingredient of strategic marketing. "The key to marketing success is market segmentation," explains Dr. Wayne Talarzyk, chairman of the marketing department at Ohio State University. "You are not trying to be all things to all people. When you start talking about Chuck E. Cheese, if you don't like pizza, you shouldn't go there; and if you don't like electronic games, you
shouldn't go there. It's a narrower approach to marketing than McDonalds."

Only recently have the pizzaria chains added microcomputers to their bills of fare. The addition of computers represents both a desire to add freshness to the entertainment cycle and an educational element to the arcade-like atmospheres. Despite the fact that computer games account for only one percent of ShowBiz's sales, they represent an important aspect of strategic marketing. OSU's Talrazyk feels that the sophistication and variation of computer software will be essential to the extention of the lifecycle of the family entertainment center. Similarly, Polack states, "I think the computer is going to be a tremendous factor throughout the life of ShowBiz and other businesses."

This action also represents an attempt to overshadow some of the bad press currently associated with video games, with the positive image associated with microcomputers and the sense of social responsibility inherent in the teaching of computer literacy. According to Rick Faulk, president of North East Venture Group Inc., the producers of ShowBiz's software, a prime reason for integrating computers into ShowBiz was to improve the restaurant's image. "The computer has developed an image that is contrary to video games," he explained. "There is a lot of negative press out on video games and a lot of positive press on computers."

![header](/img/pizza-parlor-computing3.png)

In late March the San lose, Calif., and Sunnyvale, Calif., Chuck E. Cheese restaurants began offering computer literacy classes. "Nolan Bushnell is really interested in computers and having kids learn it at an early age," states John Porter, director of communications for Chuck E. Cheese. A $10 introductory computer club membership includes a 90-minute introductory computer course, a personal diskette, and a coded membership card which allows access to the computer learning center. Six-week courses are also offered in Logo, beginning, and advanced programming. By nature the Logo course is for younger people, but the other courses are for those aged twelve and up. According to teacher Jerry Davis, student interest is very high. "It's the kind of class where they come early, and I have to boot them out," says the computer instructor from Sunnyvale, Calif. Davis, who teaches computer skills to seventh and eighth graders by day, does not feel that the Chuck E. Cheese's informal environment has any adverse effects on learning. "I don't see that it makes the least bit of difference between that environment and my class-type of environment," explains Davis. "Usually when people are working on a computer project they're so intense and their concentration is so high, that they're not distracted by outside things." The computer learning center is housed in a glass-enclosed, sound-proof room. Though students are too preoccupied to gaze outward, it is not uncommon for diners to peer in.

Members of the Chuck E. Cheese computer club can use the computers during non-class time on a token or a quarter basis. Free tokens, obtained through meal purchases or in-store birthday parties, are equivalent to five minutes of computer time. Computer users can choose from a variety of education-oriented computer games or bring their own software. "They can come in with their own diskette and practice what they learned in class, or adults can use word processors or VisiCalc or whatever programs they have," Davis explains. Presently a number of math and concentration-type games are available for public use. Davis reports that the company plans to keep at least three computers available for off-hour use. Plans are underway to open computer learning centers in company-owned restaurants across the country.

Software producer Rick Faulk of North Andover, Mass., believes that the computer games teach children many important computer skills. "They are interacting with the keyboard, and they really learn how to make a machine work," he commented. According to Faulk, the underlying concept behind the ShowBiz Computer Fun Fair is to combine fun and enjoyment with learning activity. "Through the use of full color graphics and sound, children are exposed to computers and basic programming techniques," says Faulk. "The learning games in the package are designed to challenge children's minds through play."

In December of 1982, two ShowBiz Computer Fun Fair units were installed in each of the company's entertainment centers. Each unit is comprised of an Apple II microcomputer, video monitor, disk drives, specially designed circuit cards, and software packages for ten pre-programmed learning games. Polack views the Computer Fun Fair as a supplement to the video games, as opposed to a direct competitor. "Many of the video games have a degree of violence in them. We are not directed toward that type of entertainment," explains ShowBiz's vice president. "The Computer Fun Fair is something not only where reaction time is important, but the involvement of the human mind becomes an issue also." Unlike most video games which require lightning reflexes, these games encourage learning skills such as basic number recognition, spelling, line geometry, creativity, reading, and economics.

Although the computers have only been in the stores for a relatively short period of time, children are quickly finding their way to their favorite games. Faulk reports that *The Incredible Talking Machine* is running away with everyone's tokens. A beginning level game, *The Incredible Talking Machine* teaches phonetics and sentence structure by having the computer say aloud a sentence typed into the keyboard. However, the computer is a bit old-fashioned. If a child types a four-letter word, the computer will reply, "Billy Bob will not say that." No amount of teasing will make him change his mind.

Other games such as *Find Your Number, Tic Tac Toe,* and *Minus Mission*, teach basic mathematical skills. Lemonade Stand allows one's entrepreneur spirit to shine through. Here one must make basic decisions regarding the running of a small business — a lemonade stand. The player is asked to make certain cost/price assumptions by calculating overhead expenses, determining the quantity of lemonade to be produced, and setting the price to be charged per glass. The lemonade entrepreneur must also anticipate other variable factors such as neighborhood special events and weather conditions. (Those who do exceedingly well are asked to become ShowBiz managers.)

![header](/img/pizza-parlor-computing4.png)

*Face Maker*, ShowBiz's second most popular computer game, teaches symbolic representation of actions and the correspondence between facial expressions and written expressions. By entering simple instructions, the child is able to make the face smile, wink, frown, cry, stick out its tongue, or wiggle its ears. The actions are, of course, accompanied by the appropriate sound effects.

Children love the computer games almost as much as they love pizza. In fact, for some it's difficult to decide what to do first. It's not uncommon to see children take a few bites of their pizza, run to play the computer games for a few minutes, have some more pizza, and run back to the computer. So far no one has complained that crumbs in the computer are a serious problem. But as the popularity of the computer games escalates, the crumb problem mav also escalate.

Although ShowBiz's Computer Fun Fair is child oriented, Jim Shields, manager of a Columbus, Ohio unit, reports that adults also like to experiment with technology. In fact, the Fun Fair is quite popular with college students taking introductory computer science courses. And Chuck E. Cheese's computer instructor Jerry Davis has also found adults to be very receptive to his courses.

![header](/img/pizza-parlor-computing5.png)

What new high-tech tricks do the stars have in store for the family entertainment center of tomorrow? Perhaps 1984 will see pizza connoisseurs connected by on-line networks. Image being able to communicate with your CB buddies while dining out.

In the realm of family entertainment centers, Chuck E. Cheese and ShowBiz are admittedly each other's major competitors. Clearly, the initial expense and the vast operating expenses eliminate the family-owned pizzaria from the running. However, other chains are giving them a run for their money. Castle Entertainment Inc. , of Westlake Village, Calif. , is also franchising family fun via a small chain of Tex Critter's Pizza Jamoboree theater restaurants. Aside from offering the usual education-oriented computer games, their computer learning center offers simulation programs which place people in various situations and challenge them to respond. "For instance, the computer will set up a situation where a guy is trying to meet a girl and can break the ice by delivering a witty line or a stupid one," says Bill Rameson, chairman of the board of Castle Entertainment Inc. "The computer would applaud a particularly clever line, penalize a bad one, and periodically provide a rating of the performance." Of course, your girlfriend and the computer may not agree as to what constitutes a clever line. Other simulation programs assist employees in finding the right words to ask the boss for that big raise.

Francine Sevel is a free-lance writer from Columbus.
