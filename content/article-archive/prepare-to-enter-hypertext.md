---
title: "Prepare to Enter Hypertext"
date: 2024-12-14T00:38:05-05:00
authorbox: false
toc: false
tags:
  - "MacWEEK"
  - "hypertext"
  - "Computer Magazine Article"
---

from the June 1, 1987 issue of MacWEEK

![cover image](/img/alan-boyd.png)

by Michael Goodwin

Why is Alan Boyd’s office like a hypertext document? Because it is hard to know where to look first.

There is a high-powered Mac II work-alike (a Levco Prodigy with 4 Mbytes of RAM) driving a big, high-resolution screen displaying a complicated menu of a tour of the National Art Gallery.

Next to that, a color TV monitor (displaying the Annunciation) is hooked to a videodisc player. Across the room there are several bottles of an obscure, single-malt Scotch whisky sitting on a high shelf next to a black motorcycle helmet. On the opposite wall there is a proof copy of the high-gloss, full-color ad for which the motorcycle helmet was bought; it shows a futuristic, helmeted rider, with a color computer display gleaming in his face-shield, over the headline: PREPARE TO ENTER HYPERTEXT.

Boyd is a canny Scot who left a cushy job as manager of product acquisition for Microsoft Corp, of Redmond, Wash., to start OWL International, a small but growing multinational company — product development in Edinburgh, Scotland; sales and promotion in Bellevue, Wash.

Guide, OWL’s hypertext processor for the Macintosh, has made the biggest splash of any new product in recent memory.

A flood of good reviews greeted Guide’s release late in 1986, but in the software business, the good reviews that really count are green and fold. Ford Motor Co. is paying OWL a cool million to use Guide as the frontend of its new automotive diagnostic system, which will go into hundreds of service centers. Aldus Corp, is using Guide as the help engine for its new Mac PageMaker 3.0. And around Seattle, where Aldus is located, people are saying Alan Boyd may be the next Bill Gates.

The only hard thing about hypertext is figuring out what it is. The term, as well as the concept, was invented by computer visionary Ted Nelson, who became interested in the way we read and write. Conventional text, he decided, was “one-dimensional” — frozen into a rigid structure that moved inevitably from beginning to end. Hypertext, designed to match the way we actually think, would be multidimensional text in which any word, phrase, paragraph, picture or picture element could be instantly connected with any other element in the document — or other documents.

For some time now, Nelson has been developing an ambitious, on-line hypertext database called Xanadu. Meanwhile, with his blessings, OWL has designed and released Guide, the first hypertext processor, allowing users to create hypertext documents in which readers can mouse and click on highlighted text or picture elements, called “buttons,” to reveal the con­nected material.

“Ted has this notion of an infinite hypertext, where all information is in hypertext,” Boyd explains. “We have personal hypertext, which is basically a frontend to his backend. He has this machine, the Xanadu Box, which manages an infinite amount of information, and we have developed a terminal through which you can talk to it.”

But now that people are finally figuring out what hypertext is, and what can be done with it, Boyd is moving fast forward.

“Guide is just the tip of the iceberg,” he says, gesturing toward the videodisc display. “We have much more interesting stuff in the works. Guide 2.0 moves from hypertext into hypermedia. Information does not stop at the computer, so we must get out of the computer.”

Boyd steps over to the Prodigy. “This is just a Guide document we created to access a commercial videodisc tour of the National Gallery. You click on the Annunciation here in the menu, and up it comes. Or you can do this.” He mouses up to a heading that says “Guided Tour” and clicks; the video display blinks, clears and starts a full-motion video tour, with voice-over narration, of the National Gallery. “If you want to browse,” Boyd says, mousing to the upper-left comer of the Prodigy screen, “we have a time line here of all the paintings in the different galleries, organized chronologically, so if you want to see 20th cen­tury paintings, you click here.” The videodisc screen flicks instantly to a Jackson Pollock abstract.

“And you go, Flemish, Dutch...” The videodisc screen flashes from painting to painting as Boyd rolls the mouse around the menu.

“This is just a Guide document with userembedded commands to talk to the laserdisc player,” he says. “Very easy to write. We also have a control panel for fast-forward, frameforward and back. We can access anything you can connect to the serial port.”

Boyd was bom in Scotland 35 years ago. He was still in his 20s when he arrived in the United States, where he worked at a number of odd and interesting jobs before landing at Microsoft in 1980. He is still officially on leave of absence from working to obtain a doctorate in physics in England.

“My job as manager of product acquisition at Microsoft was partly to keep tabs on the smartest developers, the people with real tal­ent,” he says. “I came across a little group in Edinburgh who had brought out a product called Expertise, the very first expert system for microcomputers.

“It was way too expensive — $1,800 or something — so their marketing left something to be desired, but the technology behind it was interesting. The design was beautiful. No one had figured out how to put a reasonable user interface on an expert system shell, and they did it. The same people who had written Exper­tise had spun off to form a company called Office Workstations Ltd., OWL, and they had developed this prototype of Guide.

“When I first saw the program in July 1984, I took one look and started thinking: These people have expert system capabilities, and they have just developed the first hypertext system, and if you put two and two together, you begin to see some real implications for CD-ROM technology.

“At that point, I could not convince Bill Gates that that was the way to go, so I left Microsoft and formed OWL International to market software by these guys.”

OWL is really two companies: an OEM to develop CD-ROMs for industrial applications, like the Ford contract, and a retail operation that releases products like Guide for the Mac. Also available is Envelope, a hypertext “pub­lishing” system that lets the user combine a hypertext document with a read-only version of Guide to create a single file that can be put on a disk or sent over a modem. The end user will not need Guide to read it; he or she can just click on it and go.

On June 1, OWL plans to release an MS-DOS version of Guide, running under Microsoft Windows, for $200, plus a hypertext edition of Ted Nelson’s “Literary Machines.”

Version 2.0 of Mac Guide, with full hypermedia capability plus significantly expanded capabilities, including improved features for aligning and importing graphics, will follow shortly.

“It also has an entirely new feature set called ‘Command Buttons’,” says Boyd, “which can be embedded just like note buttons. But instead of a definition popping up in a win­dow, the definition gets interpreted by a com­mand-line interpreter, and something happens. So if I send you a memo that says, ‘We are having a meeting at 3:30 to go over Frank’s spreadsheets,’ when you click on the phrase ‘Frank’s spreadsheets,’ Guide will load Excel and run Frank’s spreadsheets. We cap also do serial-port manipulation, to control a laserdisc, say. We can drive anything you can hook to your Mac serial port, including a mainframe computer.

“A lot of the problems with Guide 1.0, such as not being able to select a word and make it bold, for instance, are there for the sake of speed,” says Boyd. “If we had included that, we would be as slow as MacWrite. But now that the machines are getting faster, we can put that feature back into 2.0. It was always a trade-off. We tested four different versions of Guide before we released it, and we took fea­tures out left, right and center.

“We took more features out than we put in. We thought, ‘Bring out something that is very fast, very easy to use, does not push people around. Get it out there as an introduction to the concept of hypertext, and then, when Version 2.0 comes out, put some of it back.’”

Boyd’s enthusiasm for hypertext, and his notions about where it can take us, are catching. Clearly, he has no objection to turning a profit, but just beneath the businessman’s surface lives a visionary in disguise.

“It is all about how to present documents on a computer screen,” he says, “and that leads to very fundamental questions about what reading is, what writing is. Why do you have to spend so much time in school learning this stuff? Why is it so hard? There is something unnatu­ral about it. We spend most of our early schooling learning how to take an abstract thought out of our head and flatten it to put it on a piece of paper. The process of reading is just the opposite; you take this flat information and try to reconstruct this abstract shape. If we can get text where the shape of the information is actually preserved, and can be seen, then maybe we will have a better means of communication.”

OWL is working on several new items, but Boyd prefers to keep those under his hat. He is, however, happy to talk about the future of OWL and computers in more general terms.

“We have developed a hypertext system,” he says, “and we have developed an expert system. By integrating those systems, we will have a future product, something on your computer, which will say, ‘What is the problem, Dave?’ Hypermedia is only the interface.”

His eyes flash, making you wonder who was really behind the face-shield in the “Prepare to enter hypertext” ad.

“Computers are becoming what they were supposed to be in those 1930s science fiction stories,” he says. “Information appliances.” ■
