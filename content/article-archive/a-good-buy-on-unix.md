---
title: "A Good Buy on Unix"
authorbox: false
tags:
  - "Unix"
  - "Computer Magazine Article"
  - "Mark Williams Company"
  - "PC Magazine"
  - "COHERENT"
---

# A Good Buy on UNIX

Special Report by Dean Hannotte

from the June 12, 1984 issue of PC Magazine

The Mark Williams Company's COHERENT operating system is a rewritten version of the seventh edition of UNIX, with some extensions and enhancements. Regrettably, it has incoherencies.

The Mark Williams Company’s COHERENT operating system is a State-of-the-art microcomputer implementation of AT&T’s UNIX, without the state-of-the-art AT&T licensing fees. It is a completely rewritten version of the seventh edition of UNIX and includes some of its multi-user and multitasking abilities.

COHERENT first went on sale in 1980 for Digital Equipment Corporation’s PDP-11 minicomputer and was then rewritten to run on computers with the most popular 16-bit chips. It currently runs on PDP-11, 8086, 8088, 68000, and Z-8000-based systems.

Although at first it sold COHERENT only through OEMs, the Mark Williams Company began selling it to IBM PC users in the spring of 1983. By July 1983, the company had released Version 2.3, which is the one reviewed here. Version 2.4 was supposed to have been available before the end of last year, but as of March 15, 1984, it was still scheduled for release sometime in the near future. Mark Williams reportedly intends to stabilize COHERENT around Version 2.5 and then build several major new features into Version 3.0. These include networking and distributed processing options.

COHERENT runs on a PC-XT or a PC with a hard disk. You can partition your disk into two sections, one for COHERENT and one for MS-DOS. The system, which represents about 200,000 lines of source code, supports two additional computers attached through the serial ports.

## What You Get

The C compiler supplied with COHERENT follows all the de facto standards embodied in Kernighan and Ritchie’s classic _The C Programming Language_. It also supports the extensions mentioned in the _UNIX Programmer’s Manual_ such as enumerated data types, and the use of structures in assignment statements and as parameters to be passed to and returned from functions. It has many extra functions and verbose files accessed with the `include` commands. Less important features include such semantical filigree as the self-descriptive `void` data type.

This compiler only supports the small memory model, so the size of the C programs you can write is limited, but Mark William’s C compiler for MS-DOS is scheduled to support the large model within a few months. Perhaps, lurking in the wings somewhere is the corresponding COHERENT enhancement.

Another standard UNIX tool COHERENT includes is the `nroff` text-formatting program. With its extensive macro facilities, you can intersperse commands in your text that rival a programming language in their ability to manipulate and format your document. The `nroff` program is nearly as sophisticated as the more advanced word processing programs available for MS-DOS today. It easily outclasses _WordStar_ and  MicroSoft’s _Word_.

Also standard in COHERENT are `ed`, a line-editor similar to EDLIN, the `sed` and `diff` text editing tools, the `lex`, `awk` and `yacc` language preprocessors, an assembler, the `db` debugger, and an extremely handy help command.

Missing are the Berkeley enhancements such as the C user interface shell, modem communications, graphics, BASIC, Fortran, the SCCS source code control system, the `troff` typesetting program as well as several valuable text-formatting tools for scientific documents and many games such as Chess and Hunt the Wumpus.

Those who already have a stockpile of UNIX applications ready to compile and go are in good shape with COHERENT. However, for the rest of us, the question of how much application software is available arises. MS-DOS wasn’t much to show off to the neighbors, after all, before the advent of _1-2-3_ and _The Curse of Ra_. The Mark Williams people claim that within 6 months the company will have optional spreadsheet, database management, and conventional word processing packages available. And, of course, COHERENT is by definition an ideal tool for systems applications developers who are targeting their software for the burgeoning UNIX-compatible market.

## The Distribution Package

COHERENT for the PC comes on seven double-sided disks. Those of you who can multiply in your heads will realize that this number represents about 2.5 megabytes of code. Actually this amount is a bit small for a UNIX system; 8 megabytes is closer to the average. Nonetheless, COHERENT is about ten times the size of MS-DOS. Mark Williams saved a lot of space by omitting the on-line user manual, which is rarely even on systems with space to bum. The man command, which would display this manual on-screen if it were added at a later date, was not left out; this space was saved. Unfortunately, Mark Williams did omit the `spell` and `typo` dictionaries.

One of the most pleasing aspects of COHERENT is its splendid documentation: it has 1,000 letter-sized single-spaced pages suffused with intelligence and punctuated with examples helpful to both the novice and expert alike. _The Introduction to the COHERENT System_ alone runs some 114 pages, and the _Administrator’s Guide_, 75.

Much of this material consists of historical and literary examples and lengthy intellectual asides—good reading for a lazy Sunday afternoon.

To satisfy the purists, Mark Williams will issue the documentation for Version 2.4 in the smaller page size now standard for the PC. Unlike the gargantuan loose-leaf binders of single-sided photostats I received, all the new documentation will be typeset, mounted in three binders, and shelved in two boxes.

Documentation of this size could not be without occasional flaws. The first two pages describing the db command, for example, were reversed in my copy. A more significant problem was that the separate `yacc` tutorial referred to in the command manual is nowhere to be found. The fact is that it simply will not be available until Version 2.4 is released.

## Installing the System

The instructions for installing the COHERENT system are clear but incomplete. For instance, after you have booted the first COHERENT disk, the system will prompt you to insert each of the six other floppy disks to be copied onto the hard disk. I got an I/O error message on the fourth diskette, but COHERENT continued with loading regardless and told me to mount the next volume. At that point the screen looked like this:

```
restor: mount volume l,
type return key . . .
restor: mount volume 2,
type return key . . .
restor: mount volume 3,
type return key . . .
restor: mount volume 4,
type return key . . .
fd: error 44 20 20 27 1 8 2
(4,1): floppy error
restor: mount volume 5,
type return key . . .
```

Well is that an error message or isn’t it? I followed the instructions, and the rest of the installation continued without a halt. The display was full of reassuring messages telling me that everything was okay, including one that read, `"Your COHERENT system has been initialized"`. So I plodded on, hoping for the best. The next thing I did was check the file system with the command check `—s/dev/hd0`. But instead of the anticipated response, I got:

`Can't find check`

I restarted from scratch, got no I/O error messages, and this time the check command responded correctly.

The manual says "If your installation of COHERENT does not succeed, either your hardware is not configured correctly or the software supplied on diskettes is unreadable or inappropriate for your system." This diagnosis is logical enough, but how do you tell for sure whether your installation succeeded if the error messages are so cryptic? Oh yes, you can issue a command called `fderror` that will translate the system’s error message into semi-Engish, but you can only do that after you’ve gotten the system up and running.

Over a period of several days I reinstalled the system five times. The I/O error was not repeated, and the installations never took longer than 15 minutes. If you’re willing to be nervous for a day or so when you first put up COHERENT, it’ll probably all seem worth it in no time. I suggest, however, that you ignore the instructions for setting time zones, which are a real briar patch. Pretend you live in Chicago until you’ve mastered the rest of the system.

The instructions for authorizing new users are clear and simple. I started with two users, Dean with a password of Megalosaur, and Rusty with no password because my cat can’t type anyway. I sent messages to Rusty, and then read them to him when "he" logged on.

The first thing I wanted to try, once COHERENT was up and I’d brushed the cat off the desk, was the multitasking facility. I keyed in the following commands:

```
(sleep 100;echo slept 100 secs.) &
(sleep 200;echo slept 200 secs.) &
(sleep 300;echo slept 300 secs.) &
(sleep 400;echo slept 400 secs.) &
(sleep 500;echo slept 500 secs.) &
```


What these commands do is initiate successive, unattended background processes that wait a specified number of seconds and then print a message at the terminal saying how long they’ve waited. Each trailing ampersand tells COHERENT to execute the parenthesized commands as a background process. I would have continued with the sequence, but COHERENT spit out the last command, saying:

`Try again`

Since UNIX has a reputation for being cryptic, I knew I would have to look this one up. Unfortunately, COHERENT’s manual has no master index of error messages. Fortunately, I had read the installation instructions carefully and remembered seeing the message there. The cryptic note "Try again," it turned out, "indicates that the system reached its limit on the number of simultaneously active processes and was therefore unable to execute the command you requested."

My machine was running COHERENT with the minimum required memory, 256K, but five still seemed a small number of tasks for the system to choke on. I remembered fondly my diligent tests on larger IBM machines in which I’d scientifically determined how many superfluous tasks had to be piled on to crash each system. Breaking COHERENT’s back was disappointingly easy.

To get a picture of the status of the active processes in the system I tried the `ps` (process status) command. COHERENT replied:

`Out of core`

After a while the message "just slept 100 secs." appeared, followed by "just slept 200 secs.". Only then did the system respond to my `ps` command with:

```
UID _K
root 8K — P
Dean 34K-—sh
Dean 34K-—sh
Dean 9K — slept 300
Dean 34K-—sh
Dean 9K — slept 400
Dean 22K — ps —flam
```

Each of the sleeping background processes was hogging 43K RAM, not to mention 8K for the root and 34K for the foreground shell. Just these few tasks had put me 150K in debt. For this reason I recommend that any multi-user system should have at least 512K to play with.

I had more luck with the `bc` command (a more powerful version of the UNIX `dc`), an interactive desk calculator in which you can choose the precision as you can in the Lisp language. With the COHERENT bc, you can finally use your computer to find out what 100 factorial comes out to. I learned that it takes COHERENT only 6 seconds to calculate the 1,000th power of 2, and 8 minutes to find the 10,000th. I forgot to write these numbers down, unfortunately.

Since I finished this article over a weekend I was not able to call the Mark Williams people for help in understanding the last error message I encountered. After rebooting, which you have to do every time you bring up COHERENT, and issu-
ing the command `check -s/dev/hd0`, I got the response:

```
/dev/hd0:
3425 dup, class-—direct,
inode=365
/dev/hd0:
```

The manual doesn’t offer much help in interpreting this message, although it is evident that it is something that wants very much to become a real error message someday. The documentation suggests rebooting without issuing a `sync` command, up to three times if necessary. I rebooted five times to no avail. The manual says that this result means I might have to reinitialize my fixed disk. This prescription is not as catastrophic as it sounds, especially for a new user, since a `dos` command that copies COHERENT files to and from MS-DOS floppy disks has been thoughtfully provided. Even if a few sectors of the hard disk have become corrupt, I could probably dump the undamaged material onto a few floppies and reload it all after the reinstallation. In fact, I might even be able to do this with COHERENT-formatted disks.

## Certain Incoherencies

COHERENT as it stands today retains some of the administrative annoyances that are common in larger time-sharing systems. For instance, you must always issue a `check —s/dev/hd0 command` after powering-up and a `sync` command before powering-down. The documentation also says you should always remove disks before turning off the computer.

An occasional inconsistency has crept into the systems design. The `dos` command, for example, doesn’t allow a minus sign to precede the options string, although this ability is a UNIX standard.

A more disturbing restriction is that you can’t print a document in a background procedure unless you modify your cable to support interrupt-driven protocols rather than busy/wait logic. Somebody as hardware shy as I am is not exactly emboldened by instructions to "interchange wires ACKNLG and BUSY, which are signal pins number 10 and 11." The company blames this problem on "an IBM hardware bug," and in any case says it will be fixed in Version 2.4.

And yes, there are bugs in COHERENT. Every once in a while the system screwed up the screen attributes on my display—usually after some unusually clumsy bit of keyboard virtuosity on my part. But even so, when somebody hits the wrong key, that’s no reason for the computer to play with the brightness knob.

I have also heard complaints from other users to the effect that if you have nonstandard hardware attachments on your PC, you can expect some extra heartburn when you install COHERENT. This problem may be due partially to the fact that COHERENT bypasses the ROM BIOS for the sake of speed, losing some universality in doing so. Also, COHERENT reportedly does not drive the color graphics adaptor as cleanly as it does the monochrome.

As of last November, some PC-XT owners were having trouble installing COHERENT because of what Mark Williams officials said were undocumented changes in some XT hard disks. However, the company permitted these users to exchange Version 2.3 for Version "2.3+", which worked. The current incarnation of Version 2.3 apparently does not have the problem.

The surprising thing is not that COHERENT has bugs, but that I could find so few of them. COHERENT is a rapidly evolving product, but it has seen none of the horrific setbacks common to less carefully nursed systems.

## COHERENT of the Future

COHERENT’s future appears solid, at least based on the degree of user satisfaction I found in my informal user poll taken on the east and west coasts. Its manufacturer appears to think so too.

One interesting development is a major contract that Mark Williams Company has signed with Commodore to supply COHERENT as the operating system for a totally new line of home computers based on the Z-800 chip that will be on sale later this year. This version will run on only two floppy drives and still have multitasking and multi-user facilities.

To make COHERENT fit such a small environment certain development tools will become "options" available to be purchased later rather than part of the standard package, although the company expects that the C compiler will be standard. The Mark Williams Company director of marketing communications read me a Commodore press release that said, "COHERENT isn’t an imitation of UNIX. It actually corrects and enhances UNIX capabilities."

Certain to appear in future releases of COHERENT are a full-screen editor named `trout` and modem communications capabilities in the form of the UNIX `uucp` command. The company is also discussing a possibile version for PCs without hard disks, and a DOS emulator to let you run popular programs like _Multiplan_ under COHERENT.

As for the UNIX System V standard that AT&T announced in January 1983, or its rumored "Personal UNIX", it is unlikely that the Mark Williams people will feel obliged to follow the telecommunications giant’s lead. Company president Bob Schwartz is active in "/usr/group," the nationwide association of UNIX aficionados, and is working hard with it to define a standard for UNIX apart from that of AT&T. For this reason, the company has no current plans to incorporate popular Berkeley enhancements like the "C-Shell," either.

Many people who want a UNIX-like operating system are probably feeling cautious right now and waiting for the market to shake out. All I can say is that COHERENT, at $500, is the kind of bargain that is not likely to be around forever. As planned improvements are added, this price is bound to rise commensurately. Furthermore, if you are a registered user, when new versions are released you can get a new copy with any updated documentation for only $50, less than what Ashton-Tate charges for replacing a few bugs in _dBASE II_.
