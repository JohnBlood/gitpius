---
title: "From a Paper Boy to a Billion Dollars"
date: "2024-12-23T02:30:44-05:00"
authorbox: false
toc: false
tags:
  - "Electronics Australia"
  - "Tandy"
  - "Radio Shack"
  - "Computer Magazine Article"
---

## Charles David Tandy: 1918-1978

from the June 1979 issue of Electronics Australia

![Charles David Tandy](/img/tandy.png)

*Charles David Tandy, founder of the huge Radio Shack and Tandy chain of electronic stores, died recently at the age of 60. Just before his death, he had seen his Company’s annual turnover nudge the billion dollar mark — no mean accomplishment for someone who, as a child, had experienced the rigours of the great depression.*

by NEVILLE WILLIAMS

Charles Tandy's father, David L. Tandy, owned a small leatherwork store in Fort Worth, Texas and reject off-cuts helped supplement his schoolboy son's income from selling papers on the streets. Young Charles would teach his mates how to make leather belts and other knick-knacks — thereby "creating a demand" — and then supply them with the raw material they needed!

Some time later he got a real job — in the basement of a department store selling ladies shoes. In later life he would characteristically remark: "If you could sell ladies shoes, you could sell anything!"

He was sent to college on borrowed money and promptly rewarded his father by failing the course. But the experience taught Charles Tandy that, to succeed, he would have to work. And work he did, for the rest of his life! In 1940 he graduated from the Texas Christian University, entered the Har­vard Business School and, a year later, joined the U.S. Navy, serving as an officer for the remainder of World War II. While in the Navy, he set a record for selling war bonds and it proved to be an indicator of what was to come: It sharpened his business sense and gave him a feel for large sums of money even if, at the time, it belonged to other people!

After the war, Charles Tandy returned to the family business in Fort Worth and set about to expand and diversify its activities. In the process, in 1963, he made a vital decision: to purchase "Radio Shack", a chain of nine retail stores in the Boston area, with a marker value of about $2,500,000.

Facing the new challenge, Tandy plotted a unique and deliberate course. Recognised giants of the American radio scene, like RCA and G.E. were turning away from retailing to concen­trate on large scale manufacturing and distribution. Surely that would open the way for a specialist retail chain that could become known nationwide. The larger the chain, the greater would be its buying power and the keener its pricing.

The chain would bulk-buy, import, arrange manufacture, provide service and do whatever else was necessary but the emphasis would remain firmly on retail stores and the customer.

Since then, the 9 original stores have multipled and spread across America and overseas. At the last count, there was something like 4200 company owned stores handling Tandy brand products, plus nearly 3000 associated retail outlets. Corporation turnover has grown apace, to hit the billion dollar mark in the year ended June 78. For Charles Tandy, it was an ambition that was fulfilled right on target, at age 60.

But while the Tandy empire expanded worldwide, Charles Tandy's personal loyalties remained centred in the Fort Worth area and especially around his old university. It was no accident that the twin towers of the multimillion dollar Tandy Centre, commenced in 1975, served to revitalise the downtown area of Fort Worth, very close to where his father established the original leathework business. When the project is complete, it will have grown into a whole new business centre involving eight city blocks.

In retrospect, there is no doubt that the Tandy dream changed the face of radio marketing in America.

An editorial in the February issue of "CQ" magazine recalls the pre-1960 era, when amateurs were able to browse through any number of small, independant radio shops. The fashion, in those days was to build your own gear, and the multitude of shops made it easy.

But, in the late 1950's there was a downturn in this activity, with amateurs preferring commercial equipment and the number of parts suppliers diminishing rapidly. It is difficult to separate cause and effect, says "CQ" but the downward spiral was all too ob­vious.

CQ continues: "Mr Tandy, through Radio Shack, reversed that trend. Mr Tandy got us back to building again. With over 6000 radio stores available to the amateur, it is not surprising to find numerous articles in the amateur journals keying their parts lists to Radio Shack catalog numbers.

". . . Mr Tandy was not an amateur. He never worked DX . . . never climbed a tower nor strung a dipole. But he did bring the smell of solder back to the ham shack."

In Australia, the 180-odd Tandy stores and dealerships are not so directly involved with radio amateurs. They are much more concerned with hifi enthusiasts and the electronic hobbyist. It would certainly be true to say that the attraction and accessability of Tandy stores has provided the initiative for many hesitant handymen in Australia to have a go, and do it themselves!

One of Charles Tandy's major decisions, prior to his death, was to commit the Corporation to the development and promotion of a microcomputer intended expressly for small businesses and individual enthusiasts. Out of that came the TRS-80, which has already made its mark around the world in this new and burgeoning field.

In fact, it may well prove to be a whole new reach for the Tandy Empire. The TRS-80 is being supplemented by a whole range of peripherals and the Company is well advanced in its program to open no less than 50 specialised Radio Shack computer cen­tres during the current year. Unfortunately, Charles Tandy did not live to see the result of what, at the time, must have been a quite far-sighted vision.
