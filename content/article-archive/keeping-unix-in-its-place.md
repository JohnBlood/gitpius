---
title: "Keeping Unix in Its Place"
date: "2024-08-09T01:32:31-04:00"
authorbox: false
toc: false
tags:
  - "Unix Review"
  - "UNIX"
  - "Computer Magazine Article"
  - "Onyx"
  - "PICK"
  - "IBM"
featured:
  url: /img/bob-marsh.png # relative path of the image
  alt: Bob Marsh talks to Unix Review # alternate text for the image
  caption: Bob Marsh talks to Unix Review # image caption
  previewOnly: false # show only preview image (true/false)
---
## An interview with Bob Marsh

from the December 1984 issue of Unix Review magazine

![cover image](/img/bob-marsh.png)

_Many factors have contributed to the birth of a personal UNIX market but none has been more important than Onyx System's decision to introduce a UNIX-based micro in 1980. Bob Marsh, now chairman of Plexus Computers, made that decision._

_Chances are another company would have done the job sooner or later. But Marsh's timing was critical. The success of the Onyx product showed not only that a UNIX micro port was technically feasible but commercially viable. The object lesson was not lost on OEMs , who were casting about at the time for alternatives to expensive minicomputer systems._

_The rest, as they say, is history._

_Bearing this in mind, UNIX REVIEW asked Dick Karpinski, manager of UNIX services at UC San Francisco, to solicit Marsh’s thoughts on UNIX’s role in the PC marketplace. Karpinski succeeded in digging that up — and much more._

---

**REVIEW:** What does low cost UNIX mean to you?

**MARSH:** I’ve been thinking about that lately — particularly about where UNIX fits in the world, what AT&T ought to do, and how PC-class machines fit into the picture. Is UNIX even appropriate in the PC market? What is appropriate if UNIX isn’t? I think I’ve pretty much come to the conclusion that UNIX isn’t appropriate on PC class machines. MS-DOS is, though.

**REVIEW:** Does it make sense to use UNIX on an IBM PC-AT?

**MARSH:** As a small multiuser machine, yes. But the bottom line is that UNIX came out of the minicomputer world, where resources were scarce and expensive. The idea in that world was to share resources. That was fundamentally a different kind of environment — one designed for multiple users and teletype terminals. You can’t be further away from what makes a PC successful than a teletype terminal. If you look at the sort of packages that are successful on PCs, the things that have made PCs explode in the marketplace, you’ll find that they’re video oriented, bitmap display oriented, keyboard oriented, and loaded with interactive tools.

UNIX is appropriate in multiuser environments with shared data and in instances where you’re not particular about the graphic appeal of the user interface. UNIX is essentially an interface between the kernel and your applications. There’s a lot of code in there for the file system and its performance. A lot of attention is paid to isolating users and UNIX processes from the hardware underneath. That’s a lot of protection, which makes it a time-expensive system to get through. Getting back and forth through the protections and the system call interface costs you something. In addition, there is nothing in UNIX that defines an environment accommodating a window bitmap display and mouse.

**REVIEW:** There’s not a system call library for graphics?

**MARSH:** It’s not really in the kernel at all. This is because the designers of UNIX wanted to provide an optimal system for multiple users using teletypes. But PCs are completely at the other end of the spectrum. In that environment, protection from other users just isn’t an issue. You can turn UNIX into the kind of environment that would be appropriate on a PC. But by the time you did that, it wouldn’t be UNIX anymore. It’s the same argument that can be used against MS-DOS. MS-DOS is not going to become a multiuser system. If you tried to make it into one by adding what you need, it simply wouldn’t be MS-DOS anymore.

UNIX is not a single-user system because you’ve got an RS232 pipe between you and the CPU. You can take a system like the Sun and throw enough hardware at it to get reasonable performance, but even the Sun system is not mind-blowing in terms of performance of office applications.

In fact, I just got back from a conference where we talked about office systems. One of the questions that was posed was: will MS-DOS or UNIX be the system for office systems? Basically, I don’t think either one should be. Neither is ideal. In fact, something like the Mac operating system is a much, much better system for providing the productivity tools needed for office applications.

**REVIEW:** What I’m hearing you say is that a bitmap display and a windowing system are fundamental tools for building a good user interface.

**MARSH:** Certainly bitmap displays are. I’m not so sure that window presentation is optimal. I take exception to the way some of the Mac stuff is done. I like pop-up menus instead of pull-down menus, and I like mice with more than one button. But I like the attention paid to detail in the Mac.

But what I’m really after is an operating system that presumes a bitmap display, presumes a mouse, presumes a keyboard, and provides an interface that makes use of all those entities — while still providing the sort of distance you have under UNIX, so that the hardware can change underneath you.

**REVIEW:** It used to be expensive to get 64K of memory, so we used to strive for systems that could run in 12K or even 6 or 2K. Well, now it’s on the order of $150 to buy a megabyte of RAM chips. That’s just a small fraction of the price of a machine of any capacity . A 32032 is roughly a $400 chip now, so even that doesn’t represent a very big fraction either. Does this mean that the multiprogramming facilities of UNIX could be offered in personal-sized machines?

**MARSH:** Sure. The multiuser capabilities and the file system and all that are absolutely justifiable on a very small machine — if you look downstream a year or two. Memory is going to be cheaper, the processors will be faster, and the storage capacity and low cost peripherals will be there.

**REVIEW:** You made reference earlier to the barrier between application code and kernel code. Doesn't that result in a 20 to 30 percent overhead cost?

**MARSH:** The cost isn’t as important as the style of the interface. Currently, there’s a real need for some sort of window environment and some sort of bitmap display. High responsiveness, a mouse orientation and a standard way to interact with UNIX systems in a bitmap windowing environment are also needed.

**REVIEW:** Is that a problem no one is addressing?

**MARSH:** AT&T might be. They talk about it. When I was at Cambridge a couple of weeks ago, they said that was one of the things they were going to be offering. But when? It’s a fast moving market. If Microsoft came out and put that in Xenix right now, they could probably have an impact.

**REVIEW:** Was Onyx the first UNIX vendor on micro hardware?

**MARSH:** I think so. I signed the distribution license in November of 1979.

**REVIEW:** Since then there have been a lot of trade shows, a lot of new machines, and you might say that the explosion has been almost entirely in micros and microprocessor-based systems.

**MARSH:** There are other vendors now that have strategies based on proprietary processors that can offer UNIX on high end machines. That’s okay. But the reason UNIX got established in 1979 in the first place was that you had 16K memories, 8-inch Winchesters, cartridge tapes and 16-bit processors just becoming available for the first time. Add to that the fact that the 8-bit world was playing itself out. People were trying to bank switch Z-80s, install hard disks and stretch out to 128K memories. Unfortunately, there was nothing to use this raft of new hardware with. The semiconductor people didn’t provide any tools or high level languages, really. They were still providing hardware emulators and Assembly language tools.

At the time we needed something that was going to take advantage of the new hardware and thus provide an alternative to minicomputers. But you didn’t have any software available. None of the software vendors were designing for multiuser environments anyway. So UNIX just dropped into place. AT&T was not an obstacle. It wasn’t particularly helpful either, but it wasn’t an obstacle.

It’s funny. When you consider what’s appropriate and what might happen over the long term with hardware, UNIX makes sense — provided that some sort of window is utilized in the user interface.

But there’s already a large installed base of PCs. People aren’t going to replace those overnight. Neither are they going to throw out all of their MS-DOS software.

My sense of where UNIX fits in is that it shouldn’t be running on the PC itself. What makes sense is to run it on another machine that the PC is connected to so that you can continue to run MS-DOS on the PC and thus make use of the applications you like, without sacrificing the functions the UNIX machine can perform. UNIX in this way can become an additional resource for the PC users.

I think AT&T is trying to displace MS-DOS on PCs. Its efforts are wasted. We might just as well settle for finding ways for coexisting with the PC, running PC-DOS on it plus something else — probably a windowing environment.

**REVIEW:** Do you see UNIX being used strictly to serve networks of PC and Macintosh operators?

**MARSH:** Sure. It all revolves around the same argument I made before. If you take MS-DOS and add to it all the things it needs to be an effective multiuser, multitasking environment, it’s not MS-DOS anymore. Likewise, if you take enough out of UNIX to make it run fast and small and offer windowing capabilities, you don’t have UNIX anymore. That’s all there is to it. Joining the two systems allows each to do what it does best.

**REVIEW:** So are yoh saying then that Lotus 1-2-3 and MacPaint are just the beginning of a broad range of user accessible software that can offer a better human/computer interface?

**MARSH:** UNIX certainly needs help in that area. I mean, the UNIX process structure, its hierarchy of files, its interprocess communications, and the simplicity of the kernel are nice ideas. But they’re only part of the overall environment necessary for making the development of applications easy. And that after all is what it’s really all about. Nobody buys an operating system, per se. And no one solves any problems with an operating system alone. It’s just a tool that allows you to develop a certain class of applications well.

If I were working to develop the next generation of standards for PCs, I would demand software that from day one was a) independently offered; and b) designed for bitmap displays, pointing devices and keyboards. The closest you come to that right now is Smalltalk. I don’t know that much about Smalltalk as a programming environment, but it is the kind of environment that presumes both graphics and multitasking.

**REVIEW:** Doesn't Smalltalk also presume a large amount of processing power?

**MARSH:** Yes, but so does UNIX. I could make the same argument about Smalltalk that we just made about UNIX in the sense that hardware costs are coming down rapidly, making Smalltalk on PCs feasible in the next couple of years. Any industry standard is an interface. It’s a line drawn between a couple of domains. On one hand is a domain of applications and application development tools. Below that is a combination of resource management and abstracting mechanisms. This, in effect, takes some hardware technology and presents an abstract machine to the body of application development tools.

Done properly, you can be isolated from the implementation, and you can also isolate the applications. UNIX presents one class of abstract machine, heavily oriented toward ASCII terminal devices, ASCII character devices, and presuming a disk drive, file system and multiple users.

**REVIEW:** You've expressed some serious reservations about UNIX and yet you're chairman of a company selling hardware running UNIX. What can we expect to see out of Plexus?

**MARSH:** Everything we do is UNIX-based. It always has been and will continue to be. All the arguments I’ve just given you don’t indicate the fundamental value UNIX has in the marketplace. UNIX offers a multiuser environment where vendor independence and technology independence are possible. UNIX will continue to reign supreme in this regard as far as I’m concerned. In the diskintensive, multiuser, supermicro market, UNIX will be the dominant force for years to come.

**REVIEW:** Isn't this specifically for folks who have already been into computing for the last 10 to 15 years — not the new wave of folks who have just gotten computers on their desks?

**MARSH:** I don’t think they’re mutually exclusive. It is true that people buying UNIX micros for the most part today are people who have come out of the minicomputer world, because they appreciate all the characteristics of supermicros. In fact, they’re the ones who are shaping this market.

But I don’t think that necessarily excludes new users. My sense is that we’re going to find ways to treat PCs as peripherals to the UNIX system, thus marrying those two sets of technologies and using each for what it’s best at. Why try to replace MS-DOS? I mean, you’re just swimming against the current.

**REVIEW:** How are the folks who have PCs at home going to get access to UNIX systems?

**MARSH:** I’m not sure they want connections.

**REVIEW:** They want electronic mail. Some of them want NetNews.

**MARSH:** You can do that over phone lines. I guess I’ve come to the point of view that UNIX won’t be an important issue to people who have a computer at home. But there is a certain class of people — software people — that appreciate UNIX for what it is. Everybody else buys a computer to do something, like balance checkbooks, generate graphics or play games. In that environment, who cares whether UNIX is a part of it? In fact, it’s a rather expensive toy, and it’s not clear that it gives you enough functionality to pay for the overhead.

**REVIEW:** What system does offer that functionality? PICK?

**MARSH:** There are substantial advantages of UNIX over the PICK system — its general purpose and openness, for instance. The PICK system may be a highly optimal environment for certain limited purposes, but you can take that environment and put it on top of UNIX. Fundamentally you’re still dealing with disks and ASCII terminals. It’s very hard to put something like a windowing system on top of UNIX — it’s awkward and kludgey. Even when it’s done efficiently, it’s typically ad hoc. Putting the PICK system on top of UNIX, though, is not ad hoc at all. It costs you some performance, but in the meantime it also buys you the option of running a word processor or a communications package. UNIX, meanwhile, brings one very important thing to the table — it’s already virtually an industry standard. It’s not difficult to see why UNIX is important in the marketplace. The fact is that it’s good enough to suffice.

**REVIEW:** And there is already multivendor support for it.

**MARSH:** That’s right. That is what keeps attracting more application software to it, thus continuing the self-fulfilling prophecy. It is literally true that UNIX is available on lots of different machines from different vendors and that people can port software from PCs to the Cray. That’s important. That’s never happened before. That isn’t to say you couldn’t do it with Smalltalk. But nobody’s done it so far.

UNIX looks pretty conventional to people who are developing compilers and the kinds of tools and applications people want. By comparison, an environment like the Smalltalk is pretty radical. You’ve got to get people to rethink on an object-oriented system. They have to alter their style and it’s difficult. The fact that UNIX is pretty conventional, I think, is one of the reasons it is so acceptable in the commercial marketplace. People understand it.

**REVIEW:** Speaking of standards in UNIX , there is an effort in /usr/group to create a “UNIX standard . ” How is that progressing, and what do you think its importance is?

**MARSH:** Well, the standards have been published, voted on and accepted. The real question is: what’s next? The Standards Committee needs to extend the document to incorporate System V extensions that didn’t exist when work on the standards began. I know the committee is looking at interprocess calls and basic operating facilities for networking. They have a number of target areas — about 10 in all — that they’re looking at.

**REVIEW:** Are they looking at graphics interfaces?

**MARSH:** I’m sure they are. But whenever you stumble across something new that has no momentum behind it, standards efforts can stagnate for a long time while technical people argue over the merits of one approach versus another.

When the standards effort first got going, everyone was behind it. Even the look-alike vendors wanted the world to settle down. System III seemed okay, so everybody was willing to put aside their technical arguments since there was already a lot of software using System III. But when you start venturing out into new territory, like graphics, I can’t imagine a committee actually being able to pull that together.

What the committee has done effectively is abstract down from UNIX what could be standardized. Basically, they took out the things that were ambiguous and implementation dependent. In doing that, they came up with an abstraction of an implementation-independent, unambiguous machine.

**REVIEW:** They sure didn't throw out much of System III.

**MARSH:** The areas they had to throw out basically concerned terminal I/O. There were certain areas they could see they could never agree on, so they stayed away from those areas.

The question is, where do we go now? While the standards were being developed, AT&T introduced System V. AT&T is pushing that very hard now — along with an applications library based on System V. The next version of UNIX apparently will support the /usr/group form of record locking.

So, on one hand, AT&T says it’s committed to being consistent with the standard, while on the other, it’s pushing System V. My sense is that System V will ultimately be the standard. I also think, though, that the /usr/group Standards Committee will have some influence on what goes into System V and how it evolves. REVIEW: It would seem that UNIX lends itself to standards since it itself has been an effective standard for the industry.

**MARSH:** Exactly. Where people have intentionally made different versions, it disturbs customers. Actually, there are more differences in C compilers than in UNIX versions. And most of the differences are annoying. They’re not the kinds of things that involve man-years of effort to get around, but they’re annoying. So, in one sense, UNIX has been standard for a long time. If you look at only the system cedi interface over time, you’ll see that it hasn’t changed a whole lot. The implementations have changed a lot, and the utilities have changed a lot. But it doesn’t make that much difference to a guy writing in the commercial world.

**REVIEW:** Is there anything that's even close to UNIX in terms of acceptance in the multiuser marketplace?

**MARSH:** The PICK system is as close as we’ve got.

**REVIEW:** And that's at least an order of magnitude away.

**MARSH:** In terms of acceptance, yes. Even IBM thinks UNIX is important. You don’t hear those people talk about the PICK system, do you? There just isn’t the same sort of vested interest in the PICK system. There are a lot of people committed to the UNIX system who have millions of dollars of venture capital and large bases of computers. That, in itself, is part of the self-fulfilling prophecy. The more people that commit to it, the more solid the commitment becomes. More and more new people come into the fold. You even have government contractors — like ITT, Bunker Ramo, CDC and all the Beltway OEMs in Washington DC — now learning how the UNIX system operates. They’re all bidding UNIX-based systems to the federal government. It’s the result of a movement that I think actually started in the 1980-82 timeframe.

A lot of people are still trying to understand it. The biggest obstacle to understanding the UNIX phenomenon is that so many people are still approaching it from a PC point of view — evaluating it in terms of sheer volume. I sat on a panel the other day with a fellow who was downplaying UNIX, saying there’s only a hundred thousand installations. I said, "It’s a billion dollar market, what do you mean, ‘only a hundred thousand installations?’ " He was talking from a PC perspective, where machine volume is the important issue, rather than dollar volume. But UNIX systems cost 10 to 20 times what the PCs cost, and you can generally count 10 to 20 times more users on them. You can’t argue that the volumes in the UNIX marketplace aren’t significant. People using UNIX to develop applications are serious commercial users who used to buy minicomputers to accomplish the same tasks they can do today on supermicros. There is no question in the minds of most OEMs that UNIX makes sense for them.

**REVIEW:** They selected it because it’s a development environment that can be used to move applications out to real customers.

**MARSH:** Yes. It’s good enough as a development environment, but it’s also a sufficient execution environment. And don’t forget that it gives them vendor independence.

**REVIEW:** As an execution environent , can UNIX be easily hidden?

**MARSH:** Most of our customers don’t use the shell. The shell isn’t part of the interface to them because they use a database system like Unify or Informix or they use a programming language to create their applications.

**REVIEW:** How does UNIX compare with other operating systems in terms of being hidable?

**MARSH:** Oh, it’s eminently more hidable — and in a cleaner way, too. That’s where UNIX shines, as far as I’m concerned.

**REVIEW:** You mean in terms of getting out of the way?

**MARSH:** Yes. The command interface in particular is so separate from the kernel that it’s easy to just take it away and put a whole new look on it, provided you stay within the domain of ASCII character devices. It’s also very easy to replace it altogether.

We designed a newspaper system in 1975 that was based on UNIX, originally Version 5 and then Version 6. It was a dual 11-70 failsafe system. The editors never knew UNIX lived underneath it. We went around the file system and created a fixed database environment. We scrapped the shell and all the utilities, and put our own front end on it. As far as the editors of the paper were concerned, this was just an editorial system. When they logged on, they got a different look than most UNIX users do. They could move around in desks and folders and edit copy — without ever knowing they were running on top of UNIX.

In putting that system together, we found UNIX to be a beautiful development environ ment and a good execution environment in the sense that we had the option of doing whatever we wanted. We made the editing system failsafe. We developed a high-performance, robust database for it and piped wire services in with a communications front end. All of that was easy in the context of the UNIX architecture.

Graphics, in the same way, can be incorporated fairly easily, but somebody has got to do it and make it standard before it’ll have much impact.

**REVIEW:** The first microbased UNIX system on the market was the Onyx machine with the Z-8000 in it. How long did it take you to do that port?

**MARSH:** We had UNIX online three months after we had working chips.

**REVIEW:** That's moving fast.

**MARSH:** We did a lot of things fast. We didn’t have much money.

**REVIEW:** What have been the significant events in the world of microbased UNIX systems since then?

**MARSH:** Dozens of new supermicro vendors started supplying UNIX systems, and so did all the major computer companies. I felt originally that we had an opportunity because the large companies were kept occupied by their major investments in proprietary systems. This whole idea of being standard and independent was given a bit of lip service on the compiler level, but not on the operating system level.

**REVIEW:** Is the notion that buyers can avoid being locked in the key development of the last five years?

**MARSH:** That’s right. That’s the thing that surprised us the most. When we first did the UNIX system at Onyx, we were looking for a multiuser system because that’s what our customers wanted. From my standpoint, the easiest way to do that was with UNIX because it was inherently multiuser and all I had to add was record locking. I figured that would be good enough for the dealer environment, the retail environment — in essence, the whole micro world.

**REVIEW:** And you were right about that, for the most part.

**MARSH:** Yes. But what we underestimated was how great the demand would be in the more sophisticated world. The truth was that when we introduced it in the micro world, the micro world yawned — not the software developers but the dealers. They wanted applications. It took a long time to get applications out. So we were running two or three years ahead of our time in that domain. Unfortunately, the dealers made up our whole customer base at the time.

But when we introduced the UNIX micro, we found a whole different class of people showing up — big, big OEMs. They had all previously built minicomputer-based systems and were fed up with being locked into proprietary technology and not getting the kind of support they wanted. Basically, they had all been at the mercy of a single vendor, and they had each made a strategic decision never to do that again.

UNIX was a unique combination of circumstances at the time. AT&T was prohibited from playing in the marketplace, but it was required to license the technology, which had been designed 10 years earlier and so was really stable and portable. Either by accident or intention, AT&T had made it available, so lots of people knew something about it. UNIX was tailor-made for 16-bit and 32-bit machines, and the peripherals at the time were getting close to what you needed to be able to run it. Memory was getting cheap enough. So, the forward thinking OEM saw where this was all headed and strategically chose UNIX as the vehicle. Mind you, they knew early on that it wasn’t perfect. But it was good enough, and the fact that it was standard, vendor independent and technology independent appealed to them.

Three years in this business is a lifetime. You can go through two generations of processors and a whole generation of peripheral storage devices. You need to be isolated from that. You need to be able to take advantage of what comes up. That is why UNIX is successful in this market — not because it’s a nifty little operating system.

There’s still another customer that we haven’t talked about yet — the end users that act like OEMs. That is, the ones that purchase computers over a long period of time, and spread their investments over a lot of computers rather than make a major investment in one computer. Anybody who buys in volume, basically, has an inherent long term interest in standards and the flexibility they afford. But one of the characteristics of an OEM-like market is that it doesn’t explode like a PC market.

The PC filled a vacuum. There was no equivalent product before, and it didn’t really threaten too much of any manufacturer’s existing business, so it was easy to play by some new rules. UNIX micros, by comparison, were
directly competitive with minis and superminis, and so they represented a threat to the bread and butter business of DEC, Data General, Hewlett-Packard and IBM. That’s one aspect. Another is that the OEMs, if they’re successful, make an investment and then spend a few years getting some return out of it. They can’t afford to keep changing the technology or rewriting their applications, so there are only certain points in an OEM’s life cycle where the decision to change can be made. In the course of a given year, not all of them will choose to do much different, but a third of them will look at it. Some percentage of that third will decide to change.

If you’re an OEM at that point, what alternative do you really see out there? What’s your safest decision? UNIX. If you’re a minicomputer OEM, that’s obvious. But OEMs that look at UNIX through a microscope can get nervous because UNIX doesn’t have the concept of records, nor does it offer record locking, good error messages, power fail, restart, and a lot of things that OEMs have come to take for granted. But they’ve got to get over those technical hurdles. It’s a question of how much emphasis they put on technology versus independence. My argument is that independence is a large factor, and that the technical blemishes of UNIX get washed out when people look at the alternatives. The strategic value of UNIX is much more important.

**REVIEW:** What fraction of the sales of these boxes are going to end users versus large companies or OEMs , which are really a different breed of purchaser?

**MARSH:** Well, I’ll tell you what our numbers are: 70 to 80 percent of our customers are OEMs. The balance are end users. For the most part, they are technical end users.

**REVIEW:** You've said a lot about the advantages of UNIX for the OEM, but why would end users be interested?

**MARSH:** When you buy a computer, you want to be able to do as much with it as possible. A vendor-independent operating system, by definition, is going to provide you with access to lots more packages over time than a dedicated system. Thus, it’s more valuable than a system that doesn’t have that kind of open-endedness.

We’ve seen the advantages of standards elsewhere. It’s happened in retail consumer environments. Sony and Phillips and others got together to define the compact disk format standard. The consumer electronics people have figured out that it’s in their best interest to arrive at standards and then compete in the open marketplace. The commercial side of the systems business hasn’t figured that out yet. But the semiconductor people have. They know that without second sources, it’s very hard to get people to use their chips.

**REVIEW:** So a merchant market is being formed?

**MARSH:** It has a lot of the characteristics of a merchant market. Customers are demanding it now because it’s possible to demand it. It used to be that a customer would listen to a minicomputer salesman give three months worth of presentations, and then go out and compare that information to the alternatives. Once he started writing code, though, it was very painful to change.

Today, the customer does a lot more independent market research. He may even do some benchmarks, or read UNIX REVIEW and UNIX/World to sort it out for himself. No longer does he have to make a long-term commitment. The customer can begin his development, and get all the sup¬ port he would get from a minicomputer company, by buying just one machine with no volume commitment. He can then spend six months or so developing his application. Meantime, there might be 10 new announcements. Five companies form and three fold. By the time the application is ready, he’s learned what his vendor is like. In the meantime, the competitor’s salesmen have been calling. So maybe he moves code to those machines to see how hard it is. Then, when it’s time to make a decision, he calls all the vendors together and runs a demo of the application package on each machine. After that, he’s ready to talk contract terms, and he knows, as customer, he’s in the driver’s seat. He didn’t hatfe that kind of option before. Vendors in this market thus cannot stop selling or stop supporting. They can’t stay behind the technology. It’s suddenly become a very demanding environment.

**REVIEW:** When you started putting UNIX on micros, a lot of this wasn’t clear. What impressions did you have at that time?

**MARSH:** I was a DEC OEM running UNIX applications. I had been an end user and I’d been jerked around by both IBM and DEC. I knew what it felt like. UNIX made so much sense from so many standpoints that I was willing to look past the technical shortcomings. If I was willing to do that, I figured a lot of other people would be too. The speed at which the market developed blew my mind. We were talking to minicomputer people who understood minicomputer architectures and wanted minicomputer class performance. So that’s what we built. Architecturally, people recognized it. It looked like what they were used to, it performed like what they were used to. What’s more, it gave them a tremendous price advantage.

I’m wondering now how to take these underlying principles and reapply them to the same market to come up with a successor. The successor we talked about earlier was Smalltalk. My sense is that there is an opportunity for another UNIX phenomenon in this market. It might come from Microsoft, but it probably won’t. They have a big MS-DOS installed base, and it’s hard for them to shift. But there is an opportunity for another set of tools to take off.

**REVIEW:** You indicated earlier that if somebody were to develop a system that could be easily ported to various microprocessors and could provide access to graphics screens and pointing devices, it would develop a new marketplace. But would that wipe this one out?

**MARSH:** No. This market is going to be around for a long time.

**REVIEW:** Is this a marketplace of niches?

**MARSH:** No. In fact, that’s a problem of the UNIX market; it’s too broad. The market has matured so quickly that you can no longer distinguish yourself by saying you supply UNIX systems. So what? Everybody else does, too.

**REVIEW:** But the OEMs still have markets of niches?

**MARSH:** Yes, basically people are doing database-intensive multiuser applications. Actually UNIX is being used in too many different ways. MassComp is doing real time data acquisition. Sun is generating bitmap displays for technical applications. These are completely different markets with completely different sets of requirements. AT&T has people from all different segments telling it what to do with UNIX. PC people want multitasking, They also want it smaller and cheaper, and they want windowing, graphics, MS-DOS compatibility, networking, object code standards, media standards, retail image and PC ports. Basically they could care less about the 68000 and National’s 32016. They want 8086 family ports. On the other end of the spectrum is a big academic community that wants UNIX put in the public domain to let people hack on it.

There are also people who would be happy if UNIX went away, or were limited to PCs. These people, typically, have a bread and butter product that they feel UNIX threatens.

So AT&T is pulled in all these different directions. Nobody makes money on operating systems — certainly not on a scale that makes sense to AT&T. I think they ought to drop the price of the kernel, unbundle all the utilities, and compete in the utilities arena. You can charge twice as much for the utilities as you can for the kernel itself, and in the process you can eliminate a lot of hassle.

**REVIEW:** Do end users care about which operating system drives these applications?

**MARSH:** No. They don’t care at all. Actually my feeling is that AT&T should stop advertising operating systems in end user trade magazines. The focus should be on applications. AT&T should set up an authorized distributor program giving other vendors co-op advertising and some incentives to talk OEMs into moving to the UNIX environment. Small companies can’t afford to do it and they’re the ones with the resources. AT&T must set the standard. It must adopt the position of an enlightened supplier. Understand that they can’t do it by themselves; there have to be alternatives and they are only one of a number of suppliers. They need to make other people successful too if they’re going to push UNIX along. When they run a big ad for UNIX, they get a lot of leads back, but they don’t know what to do with them. It’s simple: send the leads to the licensees so they can be followed up.

**REVIEW:** They get leads and don’t do anything with them?

**MARSH:** I don’t know what they do with them, but they don’t send them to us. They should be realistic and enlightened about the marketplace. Forget about the PCs. PC-DOS is it on the PCs. Find ways to co-exist with it. The MIS managers have figured out the PC world. And now they have to figure out what to do with UNIX. Where does UNIX fit in their world? They need help, and AT&T is in a position to give it. But I think they view themselves in a struggle with MS-DOS and other systems. A better approach would be to understand where UNIX fits and help people see that.

This stupid business of sending vendors nasty letters when they use UNIX as a noun instead of an adjective is an irritation to licensees. What I’d rather see is some positive incentives. Give me a rebate on this stuff and encourage me to do some training and support. Then, if I don’t do it well enough, remove my discount. That sort of positive incentive program would be a lot more effective than a legal contractual enforcement program.

**REVIEW:** While AT&T has been sorting things out, IBM has joined the UNIX fray , and with the PC-AT announcement it appears it will be a major player in low cost UNIX. Any thoughts on where it will go from here?

**MARSH:** IBM’s recent strategy has been pretty clear. They’re going to use the PC all over the place. They had the mainframe market nailed on one hand, and then almost by accident, I think, they ended up with control of the desktopmarket. I think their strategy will be to serve as many different needs out there on desks as possible and just squeeze out anybody else in between. The interesting thing is that they’ve got UNIX on the 4300, the PC, the Series 1, the 9000, and now they’re talking about XENIX on the PC-AT. That’s UNIX of a lot of different machines.

They may not have an overall strategy, but before very long they’ll be in a position to show a consistent operating system across all of their hardware, which is something they’ve never been able to do before. That’s mind-boggling.

**REVIEW:** One of the purposes of this interview is to explore low cost UNIX. Why don’t you take a stab at defining low cost?

**MARSH:** I guess when I think of low cost, I think of the smallest, most serious business computers — something like the Macintosh. That’s a low cost machine, but it’s not a toy — it’s a high quality machine that’s acceptable in a business environment with packages running on it costing $150 each. But the operating system is taken for granted, since it’s bundled in with the Mac’s low cost processor.

As for low cost UNIX, as long as it’s running in its current form, it’ll be running on machines that cost $6000 to $7000 — until the disk prices come down, or until a new generation of disk technology comes along.

**REVIEW:** It used to be that CPUs were expensive , but micros took that out of the picture. They’re down to a few hundred dollars now.

**MARSH:** Well, a few hundred dollars is still too expensive, if you’re talking about material costs. If you sell a computer for $3000, you can bet the material costs were very low, because if you sell it retail, it’s going out the door of the manufacturer, at say, 40 percent off list, maybe even 50 percent off list. So, if you’re talking about a box that costs $1500 wholesale, the manufacturer is going to have to mark it up four or five times to make any money. When you’re talking about material costs, a couple hundred dollars here and there is expensive.

**REVIEW:** Why is low cost UNIX hard to achieve?

**MARSH:** It’s just pricing, pure and simple.

**REVIEW:** Are you talking about the price of the hardware, or the price of the software?

**MARSH:** Everybody in this business prices certain hardware configurations very aggressively and then builds margins into peripherals and add-ons, so that when they sell a typical system or a fully-loaded system, they make money.

You need to do the same thing in the software world. AT&T needs to figure that out. The strategy I would use is to make the operating system free. Of course, the operating system is little more than the kernel and a few utilities.

I’d even throw in a simple shell. Then I would sell the utilities and communication packages. People in typical configurations are going to end up buying those things. I’d have to have good utilities because I’d have to be able to compete and it’s a tough game, but you can make money there in the end.

I would price the operating system very aggressively just to guarantee it was going to be the standard. Forget about making money there. This whole business of the number of users, and what the royalty amounts to is crazy. It makes for just too much record keeping, for I would much rather charge a lower fee for the kernel and more for the utilities. Let the customers sort it out. The truth is my customers wouldn’t buy 98 percent of what AT&T provides. They just don’t supply very much that commercial customers use.

**REVIEW:** So we're talking about a virtual UNIX that is just the line between the parts you don’t use below and the parts that you don't use above? You throw the rest away?

**MARSH:** That’s exactly it. But it’s a good enough line that people see a standard in it — UNIX. UNIX serves as a playing field — an imaginary surface that separates two different domains. It provides enough stability, with different alternatives in terms of engines, that people are willing to make investments in it. On the other side is the investment in the applications they are using. That’s all an industry standard is.

Everybody gets all wrapped up talking about how UNIX is unfriendly, but every operating system is unfriendly. The operating system is unfriendly in the sense that people don’t like talking to it. Well, nobody likes talking to operating systems that I know of — except programmers. And programmers love UNIX, because it’s clean, simple and consistent. ■
