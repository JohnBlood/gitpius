---
title: "Creating Software for the Farm"
date: "2024-07-16T23:50:29-04:00"
toc: false # Enable/disable Table of Contents for specific post
authorbox: false
tags:
  - "Computer Magazine Article"
  - "Today magazine"
  - "programming"
  - "farm"
  - "programmer"
---

by Dixon P. Otto

from the April 1983 issue of TODAY magazine

"I had no intention of doing anything with computers again," says Neale Bartter of Wooster, Ohio, reflecting on the time in 1974 when he gave up a computer career for farming.

"Now I spend most of my time in here with the computer," he says from the office of his turn-of-the-century home. He nodded towards the micro sitting on the desk next to him. Another sets on the dining room table in the next room, looking a bit incongruous amid the early American furniture. Others now run the farm while he concentrates on a new business: Creating computers systems and software for farmers.

Neale Bartter was raised on a family farm in Northern Ohio. He graduated from Ohio State University with a degree in agriculture economics in 1960, expecting to return to that farm. The economics of the real world dictated otherwise.

His introduction to computers came in 1962 while working toward a masters degree, again in agriculture economics, at the University of Delaware. "This was before they ever had any courses in computers," he says, "But while I was working on a masters degree, I started working with the boiler industry on linear programming." His thesis, done with the aid of a computer, was on linear programming for the boiler industry. A career in computers was born.

![Neale Bartter](/img/bartter.png)

Most of what he learned about them came on the job. He worked for the Ford Motor Company, then for a chain of newspapers in Michigan, setting up one of the first computer systems for newspapers. Then he worked with early minicomputers for a Cincinnati firm. In 1974, he left industry, returning finally to the farm. He bought a farm in Wooster with his brother. He is now sole owner of it.

He had other plans, too. "I wanted to go into business myself. What I really wanted to do was start a software house," he says, "That wasn't really the time." In 1974, no one was interested in such a futurist idea. He thought computers were totally in his past. "I didn't keep up with what was going on with computers," he says, "I didn't read magazines on them or anything." He had no intention of doing anything with them again.

Then, in late 1979, a friend invited him over to inspect his Radio Shack Model I. "I was amazed at the progress since I had left computers, "Bartter says. A week later he had one of his own.

Normally, doing a costs analysis for the production of hogs took him several evenings of work with pencil and paper. "I hadn't programmed in BASIC in years, but I started in at 9 p.m.," he says, "By 2 a.m. I had done what it took all that time to do on paper."

Such a useful tool interested others, too. A nursery-stock broker through whom he sold trees became interested in having a system set up. "He wanted something to keep track of his customers," Bartter says. In July, 1981, Bartter set up a system for the broker and, a new business for him opened.

"I've come full circle," he says. He was in the software business at last — and more. He looks over an operation, and selects the clients' hardware that will suit their particular needs. Then he writes software tailored to their particular situation. For example, a nursery had a program for employees that automatically set aside a portion of wages into a savings account. The software Bartter set up was tailored to fit this unique situation. He has set up about 10 systems since that first one. Word of mouth has been his best advertisement. The time needed to set up a system — from when he first looks over the farm operation until the computer system is in operation — takes about six months.

"When I start with a system for someone, everything has to be spelled out to them," Bartter says. Half or more of his time is spent helping the customer master the operation of the systems, sometimes nearly "holding their hand" as they learn.

Bartter writes all the software used, except for word processing. He believes his software is superior to "canned" programs for several reasons. "With my programs, the user doesn't have to look anything up in a manual — no one reads them anyway. Nothing is code driven. Everything an operator needs to know comes up on the screen."

Most of the systems are self-contained on one disk. "The user doesn't have to swap disks back and forth," he says.

"I use a much more efficient filing system," he says. He writes his programs using all random access files as opposed to serial files used in canned programs. Such a system makes programming harder for him but makes use easier for the customer, he says.

"I can change the program easily when I have to, which I couldn't do with a program written by someone else," he says, "I also have trained most of my customers to change software over the phone. For instance, I had a customer call about a program which wasn't working just the way he wanted. I told him over the phone to change a couple of the statements." Bartter is close to the day when he can send programs over the telephone, he says.

Bartter has set up a system for Range Quality Pork, of Sedalia, Ohio, one of the largest hog farms in Ohio. The operation is based on "total confinement," of the animals with 1,250 sows all kept in a 600-by-800 ft. barn. Bartter set up two computers for them. One in the office does the accounting work such as accounts payable and receivable, general ledger and cost analysis. Another in the barn tracks the day to day activities of the sows and schedules daily work flow. For example, 21 days after a sow is bred it must be checked to see if it is in heat. Each day the computer indicates which sows should be checked. The system in the barn uses a hard disk.

"I always thought floppy disks were the last word," he says, "But now it's hard disks. A floppy would never last in the dusty, dirty barn. " The hard disk works without a problem, he says.

Bartter recently has signed a contract with Agri-Management Systems of Dayton to write software for the farm, mostly accounting programs so far. The company plans to hold courses with 15 hours of instruction on computer use for the farm, he says.

"The greatest need now is for education," he adds.

"In a decade, all farmers will have a computer," he predicts, "I think the farms that are going to survive will have to have a computer, not just because they will want to have one. " Cost will not be a factor. "The systems cost less than $10,000, which is not much in comparison to other farm costs," he says. And he forsees banks, which have an interest in sustaining the farmer, providing computer services to them.

Bartter says there will be a real need for consultants to perform in many industries the services he is now doing in agriculture. In 1974, he may have been ahead of his time with the idea of starting a software house. In 1983, his consulting activities may have come just in time to aid the financially-ailing farmer.

*Dixon Otto is a free-lance writer from Cleveland.*
