---
title: "Unix at 25"
date: "2024-08-15T07:14:18-04:00"
authorbox: false
toc: false
tags:
  - "Byte"
  - "UNIX"
  - "Computer Magazine Article"
  - "history of Unix"
---

from the October 1994 issue of Byte magazine

by PETER H. SALUS

New Jersey, in the muggy summer of 1969, was the birthplace of Unix. It was born out of the frustration that resulted when AT&T's BTL (Bell Telephone Labs) withdrew from the Multics (Multiplexed Information and Computing Service) project, a joint attempt by BTL, General Electric, and MIT to create an operating system for a large computer accommodating up to a thousand simultaneous users.

The story of the subsequent growth and development of Unix is the tale of one of the major developments in computing. "[Unix] put into the realm of the user things that were just inconceivable prior to that," notes David Tolbrook, inventor of the first dynamic cursor. He adds that Unix was not so much a great advance in computing as it was a great simplification. It demonstrated that a relatively small operating system could run on multiple hardware platforms. Unix, for the first time, showed that an operating system could be portable, machine-independent, and affordable.

Some of the Unix operating system's greatest strengths, however, stem not from its simplicity but from the truly collaborative nature of its development and evolution. Rather than being the product of a manufacturer with hardware to sell, Unix grew from the desire of a few individuals to build a system that was simple, could support more than one user, and could serve as a comfortable programming environment. Other endearing attributes were forced upon Unix by the arcane attitude of its foster parent, AT&T, toward would-be playmates.

AT&T's "house rules" for Unix included no support, no bug fixes, and no credit. The strict rearing practices of AT&T contrasted with the congeniality of Unix's conception, but both had the effect of encouraging collaboration among Unix users. As a result, Unix was not only the first portable operating system but the first, and arguably only, collaboratively developed and supported operating system—a true open system.

## Family Tree

The roots of the Unix operating system go back to Multics. Although it would later become a limited success, in early 1969 the Multics operating system could barely accommodate three simultaneous users. Ken Thompson, at BTL, began working on a game called Space Travel on a Multics machine—a GE-645. Space Travel was actually a serious astronomical simulation program, not merely a game. But it cost a great deal of compute time to play Space Travel оп the GE-645 machine, and “frivolous” operating-system development efforts were deemed unjustifiable by BTL.

Fortunately, Thompson and Dennis Ritchie located a DEC PDP-7 computer that was not in use. The PDP-7 had a 340 display, but the system came with only an assembler and a loader, and just one user at a time could use the computer. On this crude and restrictive environment, parts of a single-user Unix system were soon developed: Space Travel was rewritten for the PDP-7, and an assembler and a rudimentary operating-System kernel were written.

During April, May, and June of 1969, Thompson toyed with the idea of writing a multiuser file system. "It was never down to a design to the point of where you put the addresses and how you expand files and things like that," Thompson explains. “Dennis [Ritchie], [Rudd] Canaday, and I were discussing ideas of the general nature of keeping the files out of each other's hair, and the nitty-gritty of expanding the real implementation. [And the discussions] became the working document for the file system, which was just built in a day or two on the PDP-7."

Ritchie, Thompson, and Joe Ossanna—all Multics veterans—had tried several times to convince BTL to purchase a computer for the company’s computing research group. But in 1969, most computers meant an expenditure of well over $100,000. And despite its work on file systems and tools, the research group still had no computer of its own. Efforts to get BTL to purchase а PDP-10, ог to at least partially lease or purchase a machine, were totally unsuccessful.

Ossanna then suggested the purchase of a PDP-11/20 for a text-preparation project, in part because the administration at BTL regarded text processing as something useful. As a result of the PDP-7 development efforts to date (and confidence in the value of the text-processing system to be developed), Max Mathews, director of acoustics research, agreed to chip in seed money for a system. The PDP-11 purchase was approved. Doug Mcllroy, another Multics veteran, notes that “without that helping hand from outside computer science, Unix might never have gotten beyond the fetal stage."

## Unix Development System

It was summer 1970 before the PDP-11 became available to the research group. At first, "only the processor and memory arrived; there was no disk, and there was no operating software," notes Ritchie. Nonetheless, Unix came alive soon thereafter. "Ken [Thompson] got it going before there was a disk,” he continues. “Не divided the memory into two chunks, got the operating system going in one piece, and used the other piece for a sort of RAM disk.”

Later, when the disk arrived, work proceeded on both the operating system and the text processor. “We knew there was a scam going on—we'd promised [to develop] a word processing system, not an operating system," Ritchie adds. But the text-processing project was a success, and the patent department of BTL became the first commercial Unix user, sharing the PDP-11/20 with the research group.

![Unix versions tree](/img/unix-versions-tree.png)

Unix's development at this point proceeded under several basic philosophical principles: Write programs to do one thing and do it well; write programs so they will work together; and write programs to handle text streams. These properties constitute a universal interface.

## Unix Goes Public

With several BTL staff members from outside the research group using the typesetting facilities of the PDP-11, the need to document the operating system grew. The result was the first _Unix Programmer's Manual_ by Thompson and Ritchie, which was dated November 3, 1971. A second edition, which appeared in June 1972, noted that "the number of Unix installations has grown to 10." From that point forward, the various versions emanating from New Jersey were designated (ambiguously) by edition (which referred to the manuals) and by version (which referred to the disks or tapes). See the text box "BTL Unix Editions" on page 78.

By the third edition, the number of installations was 16; by the fourth, it was "above 30"; by the fifth, "above 50." That was 20 years ago; the numbers grew too fast for listing thereafter. One of those first 16 sites in 1972 was New York Telephone (which was part of the Bell System). The first user not based in New Jersey was Neil Groundwater (who was then with New York Telephone and is now with Sun Microsystems in Colorado Springs).

Then in October 1973, Thompson and Ritchie gave a paper at the SOSP (Symposium on Operating System Principles), and the cat was out of the bag. Immediately after SOSP, other sites began requesting this new system. The first user to get a tape of the system was Lou Katz at Columbia University in Manhattan. "Cy [Cyrus Levinthal, chair of the department of biological sciences] got RKOSs [disk packs] for the department, but we didn't have a drive," Katz says. “So I drove down to Murray Hill, and Ken [Thompson] cut me a 9-track tape."

That was in July 1974; Unix was not quite five years old. Then the publication of the SOSP paper in the July issue of the _Communications of the ACM_ caused an explosion in demand for the fledgling operating system.


## Early Cooperation Among Users

The general attitude of AT&T toward Unix—"no advertising, no support, no bug fixes, payment in advance"— made it necessary for users to band together. It is important to remember that AT&T was operating under a consent decree, and it would be nearly a decade before Judge Harold Greene would issue his ruling that created the Baby Bells.

The decision on the part of the AT&T lawyers to allow educational institutions to receive Unix but to deny support or bug fixes had an immediate effect: It forced the users to share with one another. They shared ideas, information, programs, bug fixes, and hardware fixes. The first meeting of the Unix User Group—which would later become the Usenix Association—was held on May 15, 1974, in the Merritt Conference Room at Columbia's College of Physicians and Surgeons. About two dozen people showed up.

Then, a little over a year later—in July 1975—Mel Ferentz, then at Brooklyn College, issued the first Unix news report: "Circulation 37." In a very short period of time, the Universities of Waterloo and Toronto in Canada, the University of New South Wales in Australia, Queen Mary College in London, and the International Institute for Applied Systems and Analysis (Laxenburg, Austria) had all received RKOSs or tapes.

![BTL Unix Edition](/img/btl-unix-editions.png)

The AT&T lawyers, concerned with consent-decree compliance, had believed it was safe to allow universities to have Unix. Soon they decided to let two more agencies license the system: the U.S. government and The Rand Corporation, a research organization run on government funds. But this decision was the proverbial camel's nose. There were 33 institutions on Ferentz's 1975 list of users; there were 138 in September 1976, 37 of them outside the U.S. And, in 1977, Interactive Systems (Santa Monica, CA) became the first company to support Unix commercially. It was soon followed by Human Computing Resources in Toronto.

## Berkeley Software Distribution

One of the 33 institutions on Ferentz's 1975 list was the University of California-Berkeley, where Ken Thompson had been a student. In 1975 he returned as a visiting professor, bringing the latest version of Unix with him.

Arriving at the university at nearly the same time were two graduate students: Chuck Haley and Bill Joy. They were fascinated by Unix and began working on the Pascal system that Thompson had hacked together, improving it to the point where it became the programming system of choice for the students.

However, when the university's Model 33 Teletype terminals were replaced by ADM-3 screen terminals, Haley and Јоу felt frustrated by `ed`, the line editor. They took an editor called `em`—which stood for “`ed` for mortals"—that had been developed by George Coulouris at Queen Mary College in London, and they developed the line-at-a-time editor `ex`. And it was from `ex` that Joy derived `vi`, a full-screen visual editor.

## The Right Stuff

Once news of Haley's and Joy's Pascal compiler started circulating, Joy began producing the BSD Berkeley Software Distribution (see the table "Berkeley Unix Versions" on page 80). It was first offered in March 1978. The license was on one side of a sheet of paper. The tape consisted of the Unix Pascal System (“created by W. N. Joy, S. L. Graham, C. B. Haley, and K. Thompson") and the `ex` text editor ("created by W. N. Joy"). The descriptive sheet that accompanied the license stated that “Тһе distribution is a standard 'tp' format, 800-bpi magnetic tape. A 1200-foot reel is the minimum and preferred size." It cost $50.

The fact that the BSD release had a simple license agreement, credited those who produced the software, and was priced at the actual cost of the media and distribution exemplifies what was best about Unix in its first decade and what made it such a popular operating system. À typical scenario went as follows: Something was created at BTL. It was distributed in source form. A user in the U.K. created something else from it. Another user in California improved on both the original and the U.K. version, and that was distributed to the community at cost. The improved version was then incorporated into the next BTL release. There was no way that AT&T's patent-and-licensing office could control this, and the system just got better and more widely used all the time.

Bill Joy, acting as distribution secretary, sent out about 30 free copies of BSD in 1978. Working on `vi` led him to something else: optimizing code for several different types of terminals. Joy decided to consolidate screen management by using an interpreter that was driven by the terminal's characteristics to redraw the screen. Thus, `termcap` was born.

By mid-1978, enough had been done (1.е., the Pascal system was more robust and could be run on the PDP-11/34, and `vi` and `termcap` were included) that a second BSD was put on tape. Bill Joy answered the phone, put together the distributions, and incorporated user feedback into the system. He also shipped nearly 75 tapes of 2BSD. (The last version of Unix for the PDP-11 was 2.10.1, available from the Usenix Association in 1989. It was about 80 MB and cost $200— still a real bargain.)

## Unix Grows

Up to this point, Unix could be run only on a DEC PDP system. By 1977, Tom Lyon had ported some parts of version 6 to the IBM 360 at Princeton. The next year saw Ritchie and Steve Johnson (in New Jersey) and Richard Miller (at the University of Wollongong, Australia) port Unix to an Interdata 8/32 and an Interdata 7/32, respectively. The system was not quite 10 years old, but it could run on a variety of DEC machines and on the Interdatas: Portability was born. Version 7 was the first portable Unix.

By the age of 10, Unix was already in high school. In January 1979, Brian Harvey went to Lincoln-Sudbury Regional High School, in the suburbs of Boston, “to set up a computer department." He talked the school board into getting a bond issued for equipment and persuaded DEC to give the high school a massive discount. The result was that the school received equipment worth about $200,000 for a cost of just $50,000.

However, installation "took the cooperative efforts of computer scientists at severa] places, because ош PDP-11 had a type of disk drive that the original version 7 couldn't handle," says Harvey. His 15-year-olds not only solved that problem and several others, but they began writing a variety of useful programs.

![Berkeley Unix Versions](/img/berkeley-unix-versions.png)

Unix at 10 was also in use worldwide: Haruhisa Ishida introduced the system to the University of Tokyo in 1976; it was in use in several Australian universities; and there were many sites in the U.K., the Netherlands, Germany, France, Denmark, Austria, and Israel. All this was accomplished with no advertising or support.

Version 7 of Unix, which came from BTL in June 1979, offered several major improvements. It accommodated large file systems, did not restrict the number of user accounts, and had improved reliability. Unix 7 had tremendous influence because of this and because of the number of new commands it contained: `awk`, `int`, `make`, and `uucp`, for example. The programmer's manual for the seventh edition had grown to nearly 400 pages, and it was accompanied by two 400-page supplementary volumes. This version of Unix also contained a full Kernighan and Ritchie C compiler; a far more sophisticated shell, `sh` (the Bourne shell); Dick Haight's `find`, `cpio`, and `expr`; and a large number of `include` files.

## Commercializing Unix

The Unix industry also blossomed from version 7. This release gave rise to several Unix ports: 32-bit implementations, as well as Xenix2, a collaboration of Microsoft and The Santa Cruz Operation, which was the first Unix implementation for the Intel 8086 chip. (Xenix1 was based on version 6.) Version 7 also gave rise to Unix for the Z8000 and 68000 chips. And 32V —the port of version 7 that John Reiser and Tom London did at BTL in Holmdel, under the management of Charlie Roberts—gave rise to 3BSD in 1979.

The 10-year-old system had grown quickly: Version 7 led to more goodies than the local candy store, no matter where the programmer's store happened to be. Version 8 ported `vi` (by Bill Joy), `curses` (by Ken Arnold), and `termcap` (by Joy) from BSD. Arnold's `curses` was yet another example of the influence of games on software development: It's a screen handler and optimization program that Arnold wrote to make the playing of Rogue easier.

But as useful as version 7 was, it also was quite irksome, but not because of the code—far from it. Rather, as Andy Tanenbaum of the Free University in Amsterdam puts it, "When AT&T released version 7, it began to realize that Unix was a valuable commercial product, so it issued version 7 with a license that prohibited the source code from being studied in courses, in order to avoid endangering its status as a trade secret. Many universities complied by simply dropping the study of Unix and teaching only theory."

Tanenbaum's solution was to "write a new operating system from scratch that would be compatible with Unix" but without "even one line of AT&T code." Tanenbaum called it Minix. It was the second Unix clone, the first being P. J. Plauger's Idris.

Bill Joy left Berkeley shortly before 4.2BSD came out. He took the then-current system, 4.1c, with him to Sun Microsystems. Sun's system was ultimately upgraded to 4.2BSD after the official release. Sam Leffler then took over, finished up the last bits and pieces of 4.2BSD, and pushed that out the door.

Although Leffler took over Joy's responsibilities, he was not appointed to Joy's post and felt slighted by this. So Leffler left for Lucasfilm—at first only part time so that 4.2BSD could be completed—and Mike Karels, who had been involved with the 2.9BSD release, took over the job.

The 4.2BSD release was a great success, as Kirk McKusick, chief programmer for BSD releases 4.3 and 4.4, points out. “Моге copies of 4.2 had been shipped [in the first 18 months] than all of the previous Berkeley Software Distributions combined," he notes. Several commercial operating systems were based on 4.2; DEC's Ultrix and SunOS were the most notable. Nonetheless, there were a lot of complaints about 4.2BSD, and Karels spent most of his first year on the job tuning and polishing. It was 1983, and Unix had become an unruly teenager.

## Unix Matures

А great deal could be written about how various vendors embraced (or failed to embrace) Unix. For example, Doug Mcliroy of AT&T comments that “ІВМ and BTL managed the TSS/Unix [time-sharing system] marriage quite early on, but that had no effect on IBM, while Amdahl promptly came on board. DEC tried to ignore Unix, and still does, Armando Stettner notwithstanding. Hewlett-Packard, which, like DEC and IBM, had a proprietary operating system, took up Unix enthusiastically, as did the Japanese. And there's Sun, where Unix and hardware grew symbiotically, and which propagated BSD.”

Internally, DEC had a mind-set for its first 20 years that Stettner refers to as “NІН” —which stands for "not invented here." The fact that Unix was a product of BTL was enough to turn off DEC's engineers. Up to 1978, for most of the world Unix meant "AT&T's operating system," although at AT&T Unix was a "telecommunications-support tool."

AT&T believed, because of the consent decree, that it was constrained to stay out of the computer business. Unix was handed off from AT&T's research division to USG—the Unix Support Group, which controlled Unix's future for most of the 1970s. Then, in 1979, Microsoft and SCO came out with Xenix2, and in 1980 Berkeley introduced 4BSD. Both were version 7 derivatives. Xenix remains a popular implementation, although it has become increasingly incompatible with others.

Judge Greene's decision regarding the Baby Bells meant that AT&T could loose its teenager upon the world, and System V soon was on the scene. The mid-to-late 1980s saw enormous growth in the number of Unix systems vendors and applications—and users of those applications. Unix broke into big business, into Wall Street, and into law firms.

The advent of the workstation, the growth of networking, and the maturity of the operating system itself have all made Unix an adult. By the time it was 21, in 1990, two rival consortia—the Open Software Foundation and Unix International—were contesting for its favors. (UI faded away last year.) Despite its age, Unix was the subject of a custody battle.

As is customary, the courts were soon involved. Unix Systems Laboratories sued Berkeley Software Design, Inc., claiming BSDI infringed on USL copyright and misappropriated trade secrets. A preliminary injunction against BSDI was denied, with the court ruling that USL "failed to show a likelihood of success on either its copyright claim or its trade-secret claim." These claims were undermined by AT&T's distribution of early Unix source code without copyright notice.

BSDI countersued. USL sued the Regents of the University of California; the Regents countersued. AT&T sold Unix to Novell. Novell, after considering things for a while, dropped the suits. BSDI and the Regents then dropped theirs. Novell began to sell the Unix birthright to X/Open. OSF then changed direction, and some of the former members of UI joined OSF. And the evolution continues.

Putative standards and consortia have done nothing to calm the splintered 25-year-old. Solaris, HP-UX, AIX, Ultrix, and myriad other derivatives sit at the OSF table. In fact, Unix has influenced every operating system that is sold today. Since the late 1970s, Unix has had a profound impact on DOS, Mac OS, and Windows NT. Windowing, multitasking, and networking would not be what they are today without Unix.

Sunil Das, of City University, London, notes that "technically, Unix is a simple, coherent system that pushes a few good ideas to the limit." But let history not forget that some of those ideas had nothing to do with operating systems; they had to do with sharing, collaboration, and the userdriven evolution of technology supported by a capable, concerned pan-corporate community of developers and users.

---

Peter Н. Salus is the author of A Quarter Century of UNIX (Addison-Wesley, 1994) and is managing editor of Computing Systems (an MIT Press quarterly). He is currently working on a history of the Internet and its protocols.
