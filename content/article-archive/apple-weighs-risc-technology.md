---
title: "Apple Weighs RISC Technology for Next Generation of Computer"
date: 2024-12-13T02:10:15-05:00
authorbox: false
toc: true
tags:
  - "MacWEEK"
  - "RISC"
  - "Computer Magazine Article"
  - "Jean-Louis Gassee"
  - "CPU"
---

from the June 1, 1987 issue of MacWEEK

![cover image](/img/apple-risc-plate.png)

by John Markoff

Apple Computer Inc.’s next generation of computers may be based on a radically new microprocessor architecture that could improve performance dramatically over today’s Macintosh designs.

Such computers would be built around microprocessors combining elements of both Reduced Instruction Set Computer (RISC) and multiprocessor design onto a single silicon chip. Apple’s advanced development team is now at work using its Cray XMP supercomputer to simulate the design of the new chips.

Jean-Louis Gassee, head of the Macintosh II design team and Apple senior vice president of research and development, says Apple has a RISC project but refuses comment on whether a product based on the technology is in the works.

“It’s a very popular topic at Apple these days, but whether or not we will use it I can’t say,” Gassee says. “We’ve been working on RISC a long time, but we need to learn more about it and this is not the time for precipitous moves.”

Apple’s long-range RISC project comes at a time of new interest by manufacturers in developing low-cost, high-performance RISC workstations. Sun Microsystems Inc. of Mountain View, Calif., is readying a RISC version of its workstation for introduction this summer, and Hewlett-Packard of Cupertino, Calif., is reported to have a low-cost workstation version of its Spectrum RISC architecture in the works.

RISC proponents say simplifying microprocessor design allows computers to execute basic functions faster, greatly boosting perfor­mance.

If the multimillion dollar gamble pays off, a RISC multiprocessor could allow Apple to leapfrog the competition with a PC, not two or four times faster than today’s 32-bit microprocessors, but 20 or 50 times faster — power comparable to IBM’s Sierra-class mainframes.

Apple’s RISC gamble is an important one because personal computer designers are beginning to bump against fundamental limits. Quantum leaps in performance will be much more difficult to extract in the future.

In the past, simply increasing the bandwidth of a microprocessor chip - from first 8, then 16 and now 32-bit microprocessors — ensured dramatic performance gains. Today, semiconductor engineers gain speed by “shrinking” microprocessors, placing transistors closer by using advance fabrication techniques.

RISC advocates say they can make another dramatic jump in speed by simplifying processor design. They say Complex Instruction Set Computers, like Digital Equipment Corp.’s VAX line of computers, burden computer performance with unnecessary features.

RISC designers say computers spend most of their time executing simple tasks repetitively. To boost computer speed it is best to strip away frills and concentrate on executing the most basic functions as fast as possible.

Apple has yet to produce any working prototypes of a RISC chip. Just as giant aerospace companies such as Boeing and McDonnell Douglas use supercomputers to simulate the performance of their new jets before they ever begin production, Apple is using its Cray to model a RISC computer entirely in software. The computer architecture Apple is simulating was developed in IBM’s research labs and was later picked up by computer scientists at the University of California at Berkeley and Stan
ford University in Palo Alto, Calif.

More recently, several commercial computer designs based on RISC principles have appeared: Small Silicon Valley companies have adhered more or less to the RISC philosophy.

They have been joined with RISC-based machines by major corporations such as IBM and Hewlett-Packard. IBM’s PC RT includes some of the ideas found in 1975 IBM research done by John Cocke, and HP’s Spectrum series constitutes a major revamping of its line along RISC designs.

Will Apple take a risk on RISC? Observers say there is a big difference between anadvanced-technology research project and a commercial machine. But the evidence is that Apple is looking seriously at the technology as a way to break away from the pack in computer performance

## RISC: The definition you won’t find in Webster’s

What RISC actually is remains a matter of intense debate. But now that RISC has become a new buzzword associated with greater computer speed, virtually everyone is trying to jump on the bandwagon.

David Patterson, a computer scientist and RISC designer at the University of California at Berkeley where the term was coined, says he recently attended a computer science symposium where a Motorola spokesman argued that the company’s 68030 chip was designed along RISC lines.

Patterson, an industry consultant who has been a leading RISC defender in the RISC debates, says he feels that such a designation is a misuse of the concept. However, he says there are underlying RISC principles.

First, and most obviously, RISC machines have simple instruction sets. Machine instructions — or assembly language — are the basic commands with which programmers use to command the processor.

Second, RISC designs must execute one instruction for each processor clock cycle. This is in contrast to CISC designs based on microcode, an underlying set of instructions deep in the bowels of a microprocessor, that may require more than a dozen machine cycles for instruction.

Third, a RISC machine requires a fixed format for instructions. This simplifies the circuitry necessary for decoding the instructions. Only 5 percent to 10 percent of chip surface area is devoted to control functions in the Berkeley graduate student RISC designs, while it takes 50 percent to 60 percent of available surface area in the Motorola 68000 and the Zilog Z8000.

Finally, RISC design requires a load/store architecture. This means that the only instructions that affect the computer’s memory are simple ones that move data between memory and the microprocessor register set.

As a result, Patterson says complexity is shifted from hardware to software. RISC machines require more complex compiler programs that translate higher level computer languages into machine instructions. Programs, therefore, are larger but significantly faster, he says.
