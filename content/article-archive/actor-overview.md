---
title: "Charles Duff, ACTOR's Shakespeare"
date: "2024-01-03T00:16:43-05:00"
toc: false # Enable/disable Table of Contents for specific post
authorbox: false
tags:
  - "Computer Magazine Article"
  - "Computer Language"
  - "programming language"
  - "Smalltalk"
  - "Forth"
---

by Judy Getts

from the June 1987 issue of Computer Language

Chuck Duff is the kind of man you’d expect to run into early in the morning in a university coffee shop, styrofoam cup in hand, talking Hegel with a student.

You’d expect to see him in an art gallery rambling between the neoclassicists and linear chromatists, keen-eyed and at home, or even face-to-face in a folk-jazz cafe if you motioned the waitress aside and asked who is the James Beard behind the watercress crepes.

Duff is not a man you’d expect to be a programming languages developer.

“Most of my background doesn’t relate to computers,” he admits matter-of-factly, without a trace of apology.

His interest in microcomputers was fired initially by the possibility of using them to play battlefield simulation games. That was in 1977.

Prior to that Duff had been a student of comparative religions, an aspiring Ph.D. in philosophy who was talked out of graduate school by his college advisor, who’d told him he’d be hacking (in the Checker Cab sense) in four years. Duff had also worked as a French chef at some of the toniest eateries in Chicago.

The route to owning his first microcomputer—a 6800-based machine he built from a kit—and to the programming languages he’s developed for micros is equally improbable. The one common thread in Duff's journey is his allegiance to object-oriented programming. Nowhere is that loyalty more clear than with his new language for the IBM PC family, ACTOR.

“We had seen that there was a tremendous interest in the object-oriented approach if it could be done efficiently and in a way that the average programmer could understand,” he says.

“One of the difficulties with Smalltalk is that it turned into something too big and too complicated by being isolated (at Xerox) for so long. Our interest was in bringing this technology to the mainstream computing world, by making it both more efficient and more accessible.”

The result was ACTOR, an innovative interactive language with a Pascal-like syntax and a Microsoft Windows interface. It was designed to be more straight-forward to learn than its Smalltalk forebears; cleaner, more compact, and even, if you believe the ads, more fun to use. (For a review of this language, see “ACTOR Goes On Stage” in the March 1987 issue of AJ EXPERT, pp. 40-44.)

“In ACTOR we've got a minimal set of control structures that anyone can learn in an hour,” says Duff. “I would say that for an object-oriented language, its learning curve is very, very low.”

Duff and his company, The Whitewater Group, took a year-and-a-half to create ACTOR. One of the biggest hurdles, he admits, was trying to give it the feel of a conventional programming language without compromising its pure object-oriented approach.

“Other languages like C+ + introduce objects into the language, but they’ve already got this syntax that they’re starting with and they have to add objects onto this,” he says.

The result is twisted syntax. ACTOR strives for code that “people can look at and say immediately, ‘Hey, I know what’s going on here.’.”

The Whitewater Group is headquartered on a quiet, tree-lined street just off the Northwestern University campus in Evanston, Ill. The company is housed in a former university-“maintenance building whose tumbledown facade contrasts starkly with its sleekly renovated interior. The building is part of the university’s “incubator” project that gives shelter and funding to high-tech start-ups, and The Whitewater Group is one of its beneficiaries.

In his office, Duff talks about the things that led to the genesis of ACTOR—and The Whitewater Group. He’s a sandy-haired, lanky man who can talk for hours in perfect sentences—a byproduct, no doubt, of his dialectic-steeped past. The story begins around 1982, while Duff was working for a start-up software house called Kriya Systems.

In previous years he’d worked as a systems analyst with a real estate syndicate and had designed software for bank wiretransfer systems before concluding that he “was not built for the corporate milieu.” His vocation as chef had ended years earlier when he entered the University of Illinois to obtain a masters in Information Engineering, learning PDP-11 assembly under the wing of graphics guru Tom Defanti. It was then that he decided that was where his destiny lay.

At Kriya, Duff was supposed to design a new version of the company’s bestselling software package, called Typing Tutor, for the recently introduced IBM PC. He started writing it in BASIC, but after a short time grew frustrated with what he calls the “black boxes (of the language) that you can’t adjust, and can’t change how they work.”

He felt strongly that software development occurs more quickly with an interactive language, but the only one able to fulfill that criterion—and that of extensibility—was Forth.

“The beauty of Forth,” says Duff, “is that you start out with Forth, but what you end up with by the time you’ve written your application is a custom language, that is, the language of your application.”

Unfortunately, Forth wasn’t without its problems—most notably, its physical stack model which, while manageable when crafting simple applications, becomes onerous the more complex the application becomes. Its flagrant lack of conventions also bothered Duff.

“Every Forth shop goes off in a different direction and extends it in their own way,” he says. “What you end up with is this extremely parochial evolution of the language. It gets distributed out into thousands of different versions.”

He nevertheless worked with Forth for a year-and-a-half, using it to develop several different versions of Typing Tutor— until he stumbled onto Smalltalk.

“Tt just blew my mind,” he says. “This is what I’d been aiming at.”

What made Smalltalk so revolutionary was that every piece of data, from character strings to compiler, was considered an object.

Think of objects as little experts. Each has its own set of data, assigned operations, and self-contained domain. Because an object’s data is isolated from that of other objects, you can erect elaborate systems without conflict between factions.

“Tt’s like Forth in that everything is open to you (to change),” he says, “but the beauty of it is that it’s all given form and channeled into a more elegant structure by conceiving of everything as an object.”

He calls it a “beautiful way to manage complexity.”

In the ensuing months Duff immersed himself in Smalltalk, wondering, “How can I turn the Forth environment into something a lot nicer by using object-oriented principles?”

The result was Neon, a Smalltalk-Forth hybrid—more Forth than Smalltalk—for the Apple Macintosh. At the time the only development environment for the Macintosh was the Lisa-Pascal route, and Duff found that environment “incredibly unwieldy.”

Neon was his answer to writing a version of Typing Tutor for the Mac.

The language’s first user—besides Duff—was an astronomer at Yerkes Observatory who used Neon to design a remote control panel for manipulating astronomical instruments. Its window-interface simulated mechanical controls and the program communicated with the instruments via a serial link.

What Neon inherited from Smalltalk was mostly philosophy. It used conventional Forth words augmented with the Smalltalk concept of method. Not everything in the language was treated as an object, though—classes, for instance, categorize objects and hold their methods and data maps, and in Smalltalk are themselves considered objects. But while the language relied on the familiar stack model, it was more symbolic than physical, referring to objects on the stack by names rather than position.

Neon may have been one of the most inspired languages to come to micros (indeed, Duff claims it was the first object-oriented language ever sold for a microcomputer), but its fate was not so propitiously charted. A week after its first shipment, Duff left Kriya Systems, as did the company’s marketing director, Mark Achler, a former Apple employee who had mapped an ambitious public relations strategy for the language.

Kriya was in the throes of internal disarray. Neither Duff nor Achler had been paid in three months, and when the company was bought out Duff was forced to sue to regain royalties to Neon. In his low-key way he calls the episode “kind of painful.”

At that point he and Achler decided to forget Neon and formed the Whitewater Group to develop a wholly new language. The fruit of that venture is ACTOR.

Duff and Achler’s design strategies for ACTOR aimed to develop a pure object-oriented language that would be efficient, practical to use, based upon a simple syntactical model, and provide dynamic memory management.

“What I found with Neon,” he says, “was that the parts of Forth that occurred along with it were really repugnant to a lot of people. Unfortunately, Forth has been cubbyholed and gotten a bad reputation in a lot of circles. Being associated with Forth can be the kiss of death for a mainstream language.”

With ACTOR he set out to keep only the aspects of Forth that he felt were useful, like the threaded interpreter.

Duff learned other lessons from Neon that he applied to ACTOR. One was the need for dynamic memory-management.

“To do any kind of sophisticated work like simulations and artificial intelligence, automatic memory management is really essential,” he says. “In Forth, there’ve been a million different extensions for heap kinds of memory management, but nothing is standardized.”

One of the gféatest difficulties in building ACTOR, he found, was implementing a concurrent garbage collector.

“You don’t see the segmented nature of the 8086 when you’re in ACTOR,” he says. “ACTOR can support the full address space of the machine, so we had to hide the segmented architecture.”

The garbage collector that ACTOR ended up with, according to Duff, is “virtually transparent. Very rarely will you see it pause at all.”

Another important lesson he learned from Forth was the reverse-Polish notation.

“We had to get rid of that,” he states. “It was just not acceptable. It ties you to a physical stack model.”

In hunting for a new syntax, something more conventional than that of Forth and less quirky than Smalltalk’s, Duff looked at a variety of languages, from Ada to ALGOL. The most suitable, he decided, would be a distillation of Pascal and C.

In ACTOR, he says, “many things read exactly as they would in C or Pascal. For instance, a Max function—‘give me the maximum of two numbers.’ In ACTOR you say ‘max (a,b)’ just like you would in those languages.”

He was also determined to keep the language’s linguistic tree simple.

“One thing I really believe in strongly in a language is that you do not want to proliferate a lot of features. PL/I is an extreme example. It gets so unmanageable, there are so many interactions between features that after a while you’ve built a Frankenstein.”

ACTOR has just three basic control structures: basic-conditional, a general-purpose loop, and a case-type statement. Says Duff, “I wanted to keep the number of different syntactical elements and things that a programmer had to learn to a minimum.” He adds, “There aren’t any hidden tricks that will get you if you’re not careful.”

As for a windowing environment for the language, he found himself with some tough choices. He could either build one into the language, as in Smalltalk, or else hook his trailer to an existing windowing product.

“I was disturbed by the idea of throwing yet another proprietary windowing scheme at the world,” he explains.

Deciding that Microsoft’s Windows was the windowing environment most likely to catch on as a standard in the IBM PC world, he decided to build the language under that.

Although many object-oriented principles are working in Windows, which in some ways made it easy for Duff logically to plug into, it was a challenge to create an interactive programming environment under the operating system—possibly the trickiest challenge in ACTOR. The reason was simple, says Duff: “They have their model and we have ours.”

He nevertheless found the ACTOR-Windows pairing to be one of the most gratifying elements of the project.

“Tt gives you this interactive laboratory,” he says. “It’s like a chemistry set. You’ve got all of these bottles with different kinds of windows so that you can do exciting things very quickly.”

Programming with the Windows-ACTOR team, he claims, brings immediate gratification, the kind that doesn’t come with traditional languages like C that entail editing, compiling, linking, etc. °

“Tn an object-oriented language you can build up a much higher-level model, so you just send a message— ‘show —to a window and it takes care of all the details and the interface to specific mechanisms in Microsoft Windows. It’s really brought Windows alive,” he adds.

In addition to the Windows connection, ACTOR also provides run-time windows that allow programmers to forgo the Microsoft product and craft their own windowing applications.

Right now The Whitewater Group is working to bring ACTOR to UNIX and the Macintosh. Even more ambitiously, they’re assembling a generic windowing application model that will enable any ACTOR code to conform to that model so the code will run on any machine ACTOR runs on. “That’s something that objects can do,” says Duff, “because they have this logical vs. physical orientation.”

The company is also trading on its ties with Northwestern University and collaborating with researchers there to craft AI tools geared specifically to expert systems. Although they have no products on the market at present and are remaining mum about exactly what’s on the drawing board, they expect to release some products in the future.

Another ongoing project is the creation of an ACTOR class library that will be made available for free to anyone who purchases the language.

Asked about the future of object-oriented languages as a whole, Duff, as would be expected, is unequivocally bullish.


“The future’s very bright,” he states. “People are tired of doing things the way they have been doing them.”

He mentions several studies which show that an object-oriented programmer’s productivity is several times that of a programmer working with a conventional language.

“There is a learning curve associated with object-oriented languages,” he admits, “because you have to learn all these classes and what they can do. The beauty is that once you learn what one class can do, you have a technique that can be applied uniformly throughout the system. Consistency is a strong point. Once you get that under your belt, you can absorb what the system already has in it. You have to invent very little new code to do something new.”

Past objections to object-oriented languages on microcomputers have focused on the truckload of memory they demand. Indeed, to use ACTOR and Windows together, you need a system with at least 640K.

But Duff anticipates that as micros grow more powerful, RAM requirements will become less vexing. He also anticipates object-oriented languages becoming de rigueur in the artificial intelligence world.

“I think objects can bring a tremendous amount of sophistication to the AI field, and that’s just starting to be appreciated,” he says.

While many breeds of LISP invoke some object-oriented principles, Duff considers them, and the language itself, logically confused.

“One of the things I don’t like about LISP,” he says, “is that there’s a very heavy emphasis on changing the environment dynamically, for instance, changing things on the fly. You end up with a system that, after a while, you’ve no idea how it got to be the way it is.

“When you start looking at applying languages like this in areas such as the military, the handling of hazardous ingredients, or monitoring patients on operating tables, you better damn well have a good idea of what that system’s going to do at runtime.

“I think you can do-anything in an object-oriented language that you can do in LISP,” he continues, “but in a more contained, more rigorously structured manner that lends itself to explanation and examination.”

Explanation. Examination. Duff sounds like an Aquinas here, even more so when he says, “‘At this point I feel like I’m always going to be an object-oriented programmer.” He then hastens to add, “I think this approach, this philosophy, is something that’s going to be around for a very long time.”
