---
title: "Links That May Interest You"
comments: false
images: ["/img/headshot.png"]
menu: main
toc: false # Enable/disable Table of Contents for specific post
---

# My Other Sites

* [My author homepage](http://johnpaulwohlscheid.work/)
* [My religion blog](https://realromancatholic.com/)
* [Games that I created](https://johnblood.itch.io/)
* [Historical Tidbits newsletter](https://historicaltidbits.substack.com/)
* [Computer Ads from the Past newsletter](https://computeradsfromthepast.substack.com/)
* [My Github profile](https://github.com/JohnBlood)
* [My Gitlab profile](https://gitlab.com/JohnBlood)

# Tech Websites that I Write For

* [It's FOSS](https://itsfoss.com/author/john/)
* [It's FOSS News](https://news.itsfoss.com/author/john/)
* [FOSSMint](https://www.fossmint.com/author/johnblood/)
* [Klara Systems](https://klarasystems.com/)
  - [History of FreeBSD - Part 2: BSDi and USL Lawsuits](https://klarasystems.com/articles/history-of-freebsd-part-2-bsdi-and-usl-lawsuits/)
  - [History of FreeBSD - Part 3: Early Days of FreeBSD](https://klarasystems.com/articles/history-of-freebsd-part-3-early-days-of-freebsd/)
  - [History of FreeBSD - Part 4: BSD and TCP/IP – How a Game Winning Team Was Formed](https://klarasystems.com/articles/history-of-freebsd-part-4-bsd-and-tcp-ip/)
  - [FreeBSD Jails – Deep Dive into the Beginning of FreeBSD Containers](https://klarasystems.com/articles/freebsd-jails-the-beginning-of-freebsd-containers/)
  - [Tracing the History of ARM and FreeBSD](https://klarasystems.com/articles/tracing-the-history-of-arm-and-freebsd/)
  - [History of FreeBSD - Part 5: Net/1 and Net/2 – A Path to Freedom](https://klarasystems.com/articles/history-of-freebsd-net-1-and-net-2-a-path-to-freedom/)
  - [RISC-V: The New Architecture on the Block](https://klarasystems.com/articles/risc-v-the-new-architecture-on-the-block/)
  - [History of ZFS - Part 1: The Birth of ZFS and How it All Started](https://klarasystems.com/articles/history-of-zfs-part-1-the-birth-of-zfs/)
  - [History of ZFS - Part 2: Exploding in Popularity](https://klarasystems.com/articles/history-of-zfs-part-2-exploding-in-popularity/)
  - [History of ZFS - Part 3: Heading Into the Future](https://klarasystems.com/articles/history-of-zfs-part-3-heading-into-the-future/)
  - [UNIX Wars – The Battle for Standards](https://klarasystems.com/articles/unix-wars-the-battle-for-standards/)
  - [The Birth of UNIX](https://klarasystems.com/articles/the-birth-of-unix/)

---

{{< endmatter >}}
