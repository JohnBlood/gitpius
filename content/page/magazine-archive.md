---
title: "Computer Magazine Archive"
date: "2024-01-03T00:43:37-05:00"
comments: false
images: ["/img/headshot.png"]
menu: main
toc: false # Enable/disable Table of Contents for specific post
---

I consider myself a computer historian. This is a list of articles from old computer magazines that I found interesting and wanted to archive. Enjoy.

## Australian Personal Computer

+ [RISCy Business (1985)](/article-archive/riscy-business/)

## Byte magazine

+ [Unix at 25 (1994)](/article-archive/unix-at-25/)

## Computer Language

+ [80386 Promises a New Age for AI (1987)](/article-archive/80386-promises-a-new-age-for-ai/)
+ [Charles Duff, ACTOR's Shakespeare (1987)](/article-archive/actor-overview/)
+ [Inside OS/2 (1987)](/article-archive/inside-os2/)
+ [Will the Real AI Language Please Stand Up? (1987)](/article-archive/will-the-real-ai-language-please-stand-up/)

## Computers and Automation (name later changed to Computers and People)

+ [Fictional Computers and Their Themes (1962)](/article-archive/fictional-computers-and-their-themes/)

## Electronics Australia

+ [From a Paper Boy to a Billion Dollars (1979)](/article-archive/from-a-paper-boy-to-a-billion-dollars/)

## Fun with Computers and Games

+ [May the Forth Be With You (1984)](/article-archive/may-the-forth-be-with-you/)

## MacWeek

+ [Apple Weighs RISC Technology for Next Generation of Computer (1987)](/article-archive/apple-weighs-risc-technology/)
+ [Prepare to Enter Hypertext (1987)](/article-archive/prepare-to-enter-hypertext/)

## PCM

+ [Welcome to BASIC (1986)](/article-archive/welcome-to-basic/)

## PC Magazine

+ [A Good Buy on UNIX (1984)](/article-archive/a-good-buy-on-unix/)

## TODAY magazine

+ [Creating Software for the Farm (1983)](/article-archive/creating-software-for-the-farm/)
+ [Freeware (1983)](/article-archive/freeware/)
+ [Pizza Parlor Computing (1983)](/article-archive/pizza-parlor-computing/)

## Unix Review magazine

+ [A Berkeley Odyssey: Ten years of BSD history (1985)](/article-archive/a-berkeley-odyssey/)
+ [Fear and Loathing on the Unix Trail 76 (1985)](/article-archive/fear-and-loathing-on-the-unix-trail-76/)
+ [Keeping Unix in Its Place: An interview with Bob Marsh (1984)](/article-archive/keeping-unix-in-its-place)
+ [The Business Evolution of the Unix System: An account from the inside (1985)](/article-archive/the-business-evolution-of-the-unix-system/)
+ [The Evolution of C: Heresy and Prophecy (1985)](/article-archive/the-evolution-of-c/)
+ [The Genesis Story: an Unofficial, irreverent, incomplete account of how the UNIX operating system was developed (1985)](/article-archive/the-genesis-story/)

## UNIX/WORLD magazine

+ [Beyond C: Programming Languages Past, Present, Future (1985)](/article-archive/beyond-c/)

_Note: The copyright of these articles belong to the original creators and publishers. My only goal is to make these article available as historical documents._

{{< endmatter >}}
