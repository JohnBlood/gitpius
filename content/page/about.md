---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
images: ["/img/headshot.png"]
menu: main
toc: false # Enable/disable Table of Contents for specific post
---

![](/img/headshot.png)

Hi! My named is John Paul Wohlscheid. I'm a big fan of technology. I remember using Windows 3.1.1 when I was younger (pre-teens), which should date me. I wish I could write programs and become fabulously wealthy, but he can barely write basic HTML. In the meantime, I can read and write about technology. I have published several ebooks. You can check out my books on [my website](http://johnpaulwohlscheid.work/). I mainly focus on detective fiction, for now.

## What does the name of the site mean?

The name of the website is a mashup of [git](https://en.wikipedia.org/wiki/Git) and [Pope Pius XII](https://en.wikipedia.org/wiki/Pope_Pius_XII). I picked this name because I wanted something that would convey my love of technology and Catholicism.

## How was this site built?

This site was built using [Hugo](https://gohugo.io/). This site is hosted on [GitLab](http://gitlab.com/). You can see the site's repo [here](https://gitlab.com/JohnBlood/gitpius) if you are so inclined.

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

---

{{< endmatter >}}
