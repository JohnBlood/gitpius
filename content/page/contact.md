---
title: Contact me
subtitle: Drop me a line (or two)
comments: false
images: ["/img/headshot.png"]
menu: main
toc: false # Enable/disable Table of Contents for specific post
---

If you would like to contact me, use the form below. If the form is not working, you can email me at gitpius AT protonmail.com


<form name="contact" method="POST" netlify>
  <p>
    <label>Your Name: <input type="text" name="name" /></label>
  </p>
  <p>
    <label>Your Email: <input type="email" name="email" /></label>
  </p>

  <p>
    <label>Message: <textarea name="message"></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>
