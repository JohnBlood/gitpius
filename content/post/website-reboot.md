---
title: "Website Reboot"
date: "2019-08-26T20:27:02-04:00"
images: ["/img/ribbon-cutting.jpg"]
description: "Website Reboot"
tags: 
 - "update"
 - "news"
categories: 
 - "site-news"
 # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
---

![](/img/ribbon-cutting.jpg)

First, I'd like to welcome you all to the relaunch of my tech blog. This site was created in July 2016 under the name St. Isidore's Keyboard. (In case you're wondering, St. Isidore of Seville is the patron saint of computers, computer users, computer programmers, and the Internet.) The site was built with [Hugo](https://gohugo.io/) and hosted on GitLab. I had big plans for it but got very busy and was not able to act on those plans.

<!--more-->

Recently, I discovered [Netlify](https://www.netlify.com/) and was very happy with what I found. Netlify took care of all the security certificates and even warned me if an asset was delivered via http instead of https. Setting up a custom URL was a breeze. You can set up Javascript-free contact forms. On top of that, it's free.

So, I decided to buy a URL and migrate my content to Netlify. The results are what you see before you. I hope you find the content interesting and come back.

---

{{< endmatter >}}

{{< comment >}}
