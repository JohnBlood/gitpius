---
title: "An Interview with Drew DeVault"
date: "2019-08-27T06:22:40-04:00"
description: "An Interview with Drew DeVault"
tags: 
 - "drew-devault"
 - "open-source"
categories: 
 - "Interview"
 # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
featured:
  url: /img/drew-devault.jpg # relative path of the image
  alt: Drew DeVault's avatar # alternate text for the image
  caption: Drew DeVault's avatar # image caption
  previewOnly: false # show only preview image (true/false)
---

![](/img/drew-devault.jpg "Drew DeVault's avatar")

[Drew DeVault](https://drewdevault.com/) is not your usual software developer. He has a wide range of projects under his belt, including a [groundbreaking git hosting service](https://meta.sr.ht/), an [operating system for calculators](https://knightos.org/), a [tiling window manager](https://swaywm.org/), and more. He was nice enough to agree to an interview. I hope you find it as interesting as I did. Enjoy!

(Drew originally answered these question back in April, but I wanted to finish rebooting the site before I posted it.)

<!--more-->
---

Q: Could you start out by talking about your background?

A: I've been writing software for about 15 years, and today I'm happy to say that I work on free and open source software full time.  I maintain many projects of my own, and contribute to more still. My best known works are [sway](https://swaywm.org/), [wlroots](https://github.com/swaywm/wlroots), and [sourcehut](https://meta.sr.ht/). Lesser known projects include [aerc](https://git.sr.ht/~sircmpwn/aerc), [TrueCraft](https://github.com/ddevault/TrueCraft), and [KnightOS](https://knightos.org/). Outside of projects founded & maintained by myself, I also spend a lot of time contributing to other projects, such as [Alpine Linux](https://www.alpinelinux.org/), mrsh, synapse, and so on.

Q: Please describe your computer setup. What is your Linux distro of choice?

A: My main workstation is a Linux tower with four displays in various configurations, as a means of continuously testing sway in various situations, and an AMD GPU. This runs Arch Linux, mostly because a few of my projects still have issues on musl libc (issues I intend to fix). I also keep a laptop on my desk running Alpine Linux, which I SSH into almost daily to work on Alpine, and which I travel with. I also have a Librem5 prototype courtsey of Purism on my desk, again for testing sway, and a HiFive Unleashed RISC-V board, for working on the RISC-V ports of [Alpine Linux](https://www.alpinelinux.org/) and musl libc.

Leaving my desk, I also have a handful of other small boxen, a bunch of keyboards (again for testing sway), and a rack full of servers here in Philadelphia that I run sourcehut on (as well as a small space in San Francisco for backups).

Q: A couple of weeks ago I stumbled upon your announcement about the release of Sway 1.0. From there, I discovered your other projects. Let's start by talking about the project that I am sure is taking up most of your development time: sourcehut. Why create a new source code hosting site when there are hundreds (warning: made up number) of git based alternatives?

A: There's probably only around a dozen :) I started sourcehut because I didn't like any of the other options. The main issue is that none of them were email-based, which is the workflow git was designed for. Rather than cargo culting Github (e.g. Gogs/Gitea) or making up some new thing (e.g. Gerrit), I wanted to build something which embraced and elevated the way git is meant to be used.

Q: How has the response been to [sourcehut](https://meta.sr.ht/)?

A: Great! Those who already enjoy the email-based workflow are loving it, and the CI hits the mark with a lot of people, and its taking a decent foothold in the Mercurial community. I still need to improve the tooling
for email-based code review, though - that's where the skeptics get turned away.

Q: What challenges have you encountered creating sourcehut?

It's a lot of work! And a lot still remains. I'm busier than ever, but I'm also spending more time on it than ever. The most frustrating part has been all of the annoying business-related crap I have to deal with. Tax season was rough.

Q: The biggest problem with free code hosting services is that the user and his information becomes the product to pay for overhead. You have chosen to take another route and charge the user a nominal fee. Do you   think other open source projects would benefit by charging a small amount for upkeep?

A: Yep. This is especially important for sourcehut because I provide infrastructure, which costs money to maintain. If I ask users to pay for the service and calculate the fee appropriately, they can be confident
that they know where the money is coming from - their wallets, not their secrets.

Q: Last year, when Microsoft bought Github, I read an article that said that Github had built its own workflow on top of Git instead of incorporating it into GIt. The article said that this had the effect of making it hard to move a project from Github to plain Git. It looks like one of your projects [(email + git = <3)](https://git-send-email.io/) aims to solve that. Is that true?

A: Yes, that's a problem - but git-send-email.io is only a small part of the solution. What this article was likely referring to is the pull requests and issues on GitHub are stored on their servers, unlike git objects which are replicated among all users of a git repo. It's easy to take your git repo to another host, but more difficult to do with issues and especially with pull requests.

Using email is a good way of addressing the pull request issue, and git-send-email.io is designed to familiarize people with the email-based workflow. This way, the emails are archived in your mail spools and
those of everyone copied on the email - often an entire mailing list. Importing and exporting mail spools from lists.sr.ht is something I plan to do, and I have other similar features planned for making sure you're
in control of your data.

Q: You have a number of projects related to Wayland. Wayland has been around for over a decade. Why do you think adoption has been so slow?

A: Wayland the protocol has been around for a decade. This protocol is a single XML file, which is less than 3000 lines and mostly comments.  The slow part is *implementations*, which in the case of wlroots and sway
together represent over 100,000 lines of C written by over 300 people, which has to interact with the various graphics and input subsystems of Linux, themselves enormous libraries and kernel subsystems. It's a massive amount of work.

But now we've officially made it! No more waiting necessary. Sway 1.0 was released a few weeks ago, and it's a stable and powerful desktop which should be a compelling option for almost any i3 user, and new users too. It took a long time to get here because it's a lot of work and it's almost entirely done by volunteers working in their spare time.

Q: Why create a [Unix like operating system](https://knightos.org/) for graphing calculators?

A: Why not?

Q: In one of your blog posts, you mentioned that websites are becoming more and more bloated with the use of Javascript and trackers. This concern has been raised by others, such as [Bryan Lunduke](http://lunduke.com/). The problem is that when someone wants to start a website, it is much easy to plop down a Wordpress site than learn to build something from scratch. What do you think we should be to make the internet lighter?

A: I think people rely on JavaScript too much. With sourcehut I'm trying to set a good example, proving that it's possible (and not that hard!) to build a useful and competitive web application without JavaScript and with minimal bloat. The average sr.ht page is less than 10 KiB with a cold cache. I've been writing a little about why this is important, and in the future I plan to start writing about how it's done.

In the long term, I hope to move more things *out* of the web entirely, and I hope that by the time I breathe my last, the web will be obsolete. But it's going to take a lot of work to get there, and I don't have the
whole plan laid out yet. We'll just have to see.

---

{{< endmatter >}}

{{< comment >}}
