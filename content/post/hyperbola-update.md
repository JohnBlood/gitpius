---
title: "An Update on the HyperbolaBSD Project"
date: "2024-02-15T00:52:31-05:00"
description: "This new interview reveals how far the project has gotten since it was announced in 2019."
tags:
 - "HyperbolaBSD"
 - "BSD"
categories:
 - "Interview"
 # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
meta:
  - date
  - categories
featured:
  url: /img/hyperbolabsd-logo.jpg # relative path of the image
  alt: LDPL mascot # alternate text for the image
  caption: helloSystem logo # image caption
  credit: helloSystem # image credit
  previewOnly: false # show only preview image (true/false)

---

![](/img/hyperbolabsd-logo.jpg "HyperbolaBSD logo")

Back in 2019, the dev team behind the [Hyperbola GNU/Linux](https://www.hyperbola.info/) announced that they would be moving away from the Linux kernel. They decided to fork the [OpenBSD](https://www.openbsd.org/) kernel. Not only that, they planned to rewrite and replace all code that is not  GPL v3 compliant. Back in January of 2020, I interviewed the HyperbolaBSD team for [ItsFoss.com](https://itsfoss.com/hyperbola-linux-bsd/). I decided to reach out to them again to see how the project has progressed. I also included some questions from fellow nerds. Enjoy the interview.

<!--more-->

-------

**Q: Back in December 2019, you announced that you would be moving from the
Linux kernel to your own fork of the OpenBSD kernel (the Hyper Berkeley
Kernel). It's been over 4 years. How have things been progressing?
When do you think you'll have an alpha build available? When do you
think it'll be ready for day-to-day use?**

A: Since December 2019, there were many things happened along the time. One
of them was the COVID-19 pandemic, which not only affected our planning
to begin the project and our lifes, but the whole world, with loosing of
many lifes. For this reason HyperbolaBSD development begun exactly at
20th March 2022.

But in comparison to before, we have a better organization/planning how
to develop it.

For the first version, our planning have 4 stages of development:

**Pre-alpha:**
- Port the x86 system from binutils 2.17 to 2.34 (with HyperbolaBSD
patches)
- Port the x86 system from GCC 4.2.1 to 8.4.0 (with HyperbolaBSD
patches)
- Port the x86 system from GNU C99 to GNU C17 standard
 - Adapt code to be built under FreeBSD bmake

**Alpha:**
- Write compatible code under Simplified BSD License to replace the
non-free files in kernel and libc
- Modularise kernel and userspace under the Unix modular design
- Replace non-free tools/applications with fully libre third-party ones in userspace

**Beta:**
- Develop hyperman (hard fork of pacman adapted for HyperbolaBSD)
- Develop hypertools (hard fork of libretools adapted for HyperbolaBSD)
- Migrate our build-server (Laminar) towards HyperbolaBSD
- Port runit for HyperbolaBSD in order to replace the OpenBSD init
- Develop runit init scripts to run HyperbolaBSD
- Package the entire modularised HyperbolaBSD system with hyperman
- Develop the HyperbolaBSD live image
- Port [Xenocara](https://www.xenocara.org/) for HyperbolaBSD

**Release Candidate/Final version:**
- Port and package software included in our repository named
"extra" from Hyperbola GNU/Linux-libre to HyperbolaBSD, including also
an audit of being really needed or just be seen as nice addition

Currently, we are very near to launch our pre-alpha version which will
be ready for testing purposes only. There isn't an exact prevision of
the date for all of those versions, but we will let you know in our
website's news.

In other hand, the original HyperbolaBSD idea planned at December 2019
continues in the same way than before, however under a progressive
migration by replacing all non GNU GPL-compatible code along the next
HyperbolaBSD versions in the future. The difference here is about it
will be replaced with new compatible code under Simplified BSD License,
not the GNU GPLv3. We have decided for it in order to contribute for
other BSD descendant operating-system projects. But for HyperbolaBSD
side, also it will be useful as well, because we could incorporate GNU
GPL code from other projects such as ReactOS, as well new code from
scratch made by us in HyperbolaBSD.

By the way, I would to like to emphatize here that our plan never was
just to move the Linux kernel to an hard fork of the OpenBSD kernel, but
the develop an independent BSD descendant operating-system, based on
hard-forked code from different BSD descendant operating-systems (mostly
from OpenBSD 7.0), as a starting point for the development of the first
HyperbolaBSD version.

**Q: What unexpected problems have you encountered?**

A: Currently we didn't find critical problems, but yes how to port the
whole code at the beginning stage of the development, with a different
version of the C standard, as well binutils/GCC, because this part of
the development requires do a drastic change.

**Q: Have some problems turned out to be easier than you thought?**

A: For now we didn't find that possibility, but it could be probable along
the development in this first version.

**Q: In the HyperbolaBSD roadmap, you have an entry to remove non-x86 from
the kernel and libc. It appears that initially you will only support x86
systems. Do you plan to add more architectures in the future?**

A: We have plans to do it in a long-term, but there are other priorities to
do first.

**Q: In my previous interview, you mentioned that you "need C programmers and
users who are interested in improving security and privacy in software".
I know from your site that you have disagreements with Java and Rust.
Would you accept code from other languages?**

A: A base for an operating-system should be most focussed on one language.
Creating HyperbolaBSD can include C and / or C++. Regarding [Java](https://wiki.hyperbola.info/doku.php?id=en:philosophy:java_downfalls) and
[Rust](https://wiki.hyperbola.info/doku.php?id=en:philosophy:rust_trademark) especially it is not possible to include code using them especially
from the perspective of their trademarks, mainly in the redistribution
restrictions. Trademarks are from our perspective one major trap when
done very strict and not allowing further modifications even block
exactly the freedoms libre software should grant for users and
developers. We need to make a difference between abusive trademarks and
others allowing freedom.

But this is a point we want to explain a bit more deep towards the
example of Rust: They demand in the regulations within their "Logo
Policy and Media Guide" only some minor possible modifications being
allowed. Otherwise a complete rebranding of the whole
programming-language is needed, resulting in fact towards a complete
hard fork. A rebranding of a complete programming-language is different
in comparison to a rebrading of an application for endusers, web-browser
as example to be named. A rebranding cannot be done with a simple
command and needs a complete review of all components, not only binaries
like the compiler named different.

Furthermore Java following comparable strict, problematic trademarks and
with its own history of security-flaws over the years is nothing we want
to bother also. It would also distract further the focus if we would
accept more code from other languages as it would also need porting,
packaging and intense testing: Another point of distraction. So we stay
strict and straight oriented on our roadmap and plans.

![](/img/hyperbolabsd-mascot.jpg "HyperbolaBSD mascot")

***

_**Question from the community:**_

**Q: I realize this question comes off as condescending, but I mean it with
sincerity. Why not take your considerable developer time and expertise
and contribute to a project that already meets your stated reasons for
leaving the Linux ecosystem like OpenBSD? Wouldn't assisting that
project contribute more to the BSD ecosystem then yet another project?**

A: OpenBSD is already including software with freedom-issues and the ports
are doing same. The decision for another BSD-descendant operating-system
is also a decision about software-freedom. Sure Hyperbola Project
follows a right different perspective as others and that marks the
important point: Every new project adds another variety for a colourful
software-landscape regarding libre software. The major aspect for
Hyperbola Project, including HyperbolaBSD and also Hyperbola
GNU/Linux-libre, is our focus on technical emancipation: We want to
motivate users to rethink about false balancing to accept non-free
interfaces and inclusion of questionable implementations. Giving full
control over every aspect of the system users can be sure all parts do
not include any unknown firmware-blobs and also audits for issues and
flaws are way more easy.

**Q: Do they belive in the idea of perpetual development of software - or do
they perhaps instead hold that there is such a thing as "optimal
software" - software that has achieved a sort of perfection or optimum
and doesn't need to be developed anymore? Does this belief of theirs,
whatever it is, factor into their decision to commit to a ten-year
development process?**

A: That depends on the software itself and its operation area: Building an
operating-system is different towards a generic application for the
commandline or with a graphical user-interface. Software with intense
security-focus will always need perpetual development, this includes
also the kernel and the userspace. Other applications outside may reach
a point being defined as "working perfect" and so frozen in development.
So as long as we have support and can work with passion on HyperbolaBSD
we will do that as Hyperbola Project will be always community-focussed.
Anybody can use our development for a base to build as we have also
neither the interest nor the need to follow too much implications from
outside and can pronounce our own perspectives.

**Q: What areas of the project do you think need some TLC that other areas get??
How can one contribute to the project??
Who to contact at the project??
Do you need beta testers??
If so how to sign up??**

A: That depends on the individual perspective: Working on security-aspects,
replacing questionable licensed and non-free code, removing non-free
blobs and system-calls, there are many different task life cycles in
need for attention. Also the on-going packaging and providing a working
infrastructure for users doing ports. But exactly coming the last
mentioned point: Contributions are possible on many ways like writing
documentation, supporting the development for our essential
packaging-tools towards our system or also granting support for users
with porting and packaging. Therefore you can always get in touch with
us through the forums as every active team-member can help. So yes, we
also need beta-testers when entering the concrete phase. Reporting for
this will be done through announcements in our forums.

**Q: What HyperbolaBSD recommends for web development?**

A: That depends foremost not on the operating-system itself and is more a
generic question towards security: Web-development is a risky field.
Neither [NodeJS](https://www.hyperbola.info/todo/nodejs-removal/) nor others like [PHP](https://wiki.hyperbola.info/doku.php?id=en:philosophy:php_trademark) or [Java](https://wiki.hyperbola.info/doku.php?id=en:philosophy:java_downfalls) can be seen to be approved
working in the perspective of trademarks and security itself. A
recommendation is therefore near impossible while best would be to
minimize the attack-surface. Complex web-development should be avoided
generating a false perspective of being possible without flaws.

**Q: What HyperbolaBSD recommends for desktop app development?**

A: Same as before: The more simple the better especially in usage of
frameworks. Qt has proven to be not fully working as for example
including non-free Chromium partwise and licensing-model being not fully
oriented towards libre software. GTK+ is going more into the direction
being no longer oriented backwards-compatible and including more
interfaces and frameworks making maintenance even more complex over
time. While we can't give a generic recommendation the perspective is
the most important: More simplistic designed applications for
desktop-environment with a clear set of tasks is way better than
applications getting too complex. Besides we should always remember that
"app" is misusage of the wording "application", which is already the
concrete statement about a straight focus Hyperbola Project is following.

---

{{< endmatter >}}
