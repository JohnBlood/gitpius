---
title: "Why Windows Needs a Package Manager"
date: "2017-01-11T20:13:45-05:00"
images: ["/img/manjaro-package-manager.png"]
description: "Microsoft releases the code to WIndows Live Writer as open source."
tags: 
 - "Windows"
 - "Linux"
 - "Manjaro"
 - "Solus"
categories: 
 - "opinion"
 # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: true # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
---
![](/img/manjaro-package-manager.png "Pamac package manager")

A couple of days ago, I was building a blog using [Hexo](https://hexo.io/) (a static site generator built on Nodejs). I created a couple pages and installed a couple plugins. When I entered the command to build a local copy of the site, I got a nice long error message. It looked like one of the plugins was causing the problem, so I filed an issue on GitHub. It turned out that I was getting the error because my version of Nodejs was old, as in two major releases behind the most recent release.

<!--more-->

I was able to fix the problem, but it got me thinking. All this happened on my Windows laptop. It would never have happened on my Linux machines. Why the difference? Because Windows does not have a package manager. Here are several reasons why Windows needs a package manager.

![Solus Project's package manager](/img/solus-packagemanager.png)

## What is a Package Manager?

For those of you who have never used Linux, let me explain what a package manager is. A package manager is a program that allows you to update your current programs and install new programs. Package managers allow you to search for applications by name and by category. Some also list reviews of the programs from other users.

## Updating Simplified

In Windows, one of the biggest problems is keeping your software up to date. In most cases, you have to download each update and install it separately. For example, if I want to update CCleaner, you have to download the latest version and install it. Some programs, like Flash or Java, have their own update systems, but you generally still have to initiate each update.

With a package manager, there is no such problem. All you have to do is run the package manager (either from the graphical user interface or the terminal) and all updates will be installed.

## Updates All Packages

Some people might point out that Microsoft does include a package manager of sorts with Windows Update. That may be true, but it only updates Microsoft software. The non-Microsoft software that it updates is usually drivers. Remember when I mentioned Nodejs at the beginning of the post? That was not covered by Windows Update.

On the other hand, when I update my Manjaro laptop, everything is updated, from full programs like Firefox and Steam to building block like Nodejs, Go, Java and system drivers.

There are some third-party programs that check for updates, but they only search for a limited number of programs. For the record, I have heard of [Chocolatey](https://chocolatey.org/), which touts itself as a package manager for Windows. However, in my opinion, something that security sensitive should be in the hands of the creator of the operating system, in this case, Microsoft.

![Solus updated via terminal](/img/solus-update-terminal.png)

## Reduces Bloat

One of the biggest problems with getting a new computer is that it often comes with quite a bit of bloat. This usually consists of 15 or 30 days trials of software. The idea is that you will get hooked on using these programs and pay for the full version. Most people want to uninstall them, but the software makers make this hard. There are several stand-alone programs just for removing bloat from new PCs.

A package manager would greatly simplify the removal of bloat. All you would have to do is open the package manager, select the program you don't want, and hit uninstalled. However, this will never happen because PC makers make too much money from software makers my weighing down new machines with bloat.

## Software is Easy to Find

Finally, a package manager would make it very easy to find new software. Finding software has always been a problem for Windows PCs. Before the internet, you could go to your local PC store and find which software is right for you. In the Internet Age, there are so many options to choose from, you can get overwhelmed trying to make a choice. Many times I've just done a Google search for the program type I'm looking for with the hope I'll find something to fill my need.

Package managers consolidate a large portion of software into one place. You can search by name and by category. Sometimes they list reviews from other users, so you can see if the program is right for you.

## Will it Ever Happen?

Microsoft has mentioned in the past that they would include more Linux-like features in future versions of Windows. The article even mentioned a package manager. However, I think a Windows Apps store is as far as they will take it. (An app store would have many of the same problems as Windows Update.)

In my mind, that is not far enough. Hopefully, a few people will read this and realize what Windows is missing.

Let me know what you think in the comments below? Do you agree that Windows needs a package manager or not?

(Note: The image at the top of the post is the Pamac package manager on Manjaro Linux.)

---

{{< endmatter >}}

{{< comment >}}
