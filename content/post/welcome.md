---
title: "Welcome to the Show"
date: "2016-07-14T22:15:34-04:00"
description: "The first of many informative tech articles."
images: ["img/curtain.jpg"]
draft: false
tags: 
 - "introduction"
 - "new things"
categories: 
 - "announcement"
  # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
---

![](/img/curtain.jpg "Welcome to the Show")

Hi, everyone, and welcome to my new blog about computers and technology. I will be your host, John Paul Wohlscheid.

<!--more-->

This blog came about because I wanted to try creating a [Hugo-powered](https://gohugo.io/) blog. Since I love goofing around with computers, I decided to make that the topic of the Hugo blog.

## Credentials

Now, to give you some of my background with computers. My earliest recollection of a computer was a text-based system. (I don't know what the operating system was.) One of the first computers that I played games on ran Windows 3.1. Heck, I remember when my system got updated to Windows 95. That was a big day.

In college, I was introduced to the world of open-source computing, including Linux and Firefox. (I've got an official Ubuntu 7.04 install disk lying around somewhere.) Nowadays, I run [Manjaro Linux](https://manjaro.github.io) on three computers and tinker with new open-source software, as far as my hardware will allow.

## What's To Come?

This blog will cover my thoughts on computers, software, and technology. I've got a lot of thoughts and ideas, but not much time to write them up. We'll see how it goes.

# Please note

This blog is a work in progress. Things will change and features will appear without warning as I become more familiar with Hugo. :laughing: Enjoy the ride.

{{< comment >}}
