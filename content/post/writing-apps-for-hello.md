---
title: "Writing Apps for helloSystem"
date: "2023-02-16T07:25:41-05:00"
# images: ["/img/ldpl-logo.png"]
description: "Time to be a big fish in a little pond"
tags:
 - "helloSystem"
 - "Python"
categories:
 - "programming"
# Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
meta:
  - date
  - categories
featured:
  url: /img/hello_variation.svg # relative path of the image
  alt: LDPL mascot # alternate text for the image
  caption: helloSystem logo # image caption
  credit: helloSystem # image credit
  previewOnly: false # show only preview image (true/false)

---

![](/img/hello_variation.svg "helloSystem logo")

I've been wanting to learn how to program for a while. In the past couple of years, I tried many times. Different languages and different tutorials. But I always got distracted, either by another project or another language. I think one of the reasons I didn't have much luck was that I really didn't have an end goal, something to work towards. Now, I do.

<!--more-->

I am a member of the [Lunduke Locals](https://lunduke.locals.com/), and we have been doing a series of themed weeks. This week is BSD Week. So, I started looking at the different BSD projects, to see which one I'd try out. I've tried [FreeBSD](https://www.freebsd.org/), [MidnightBSD](http://www.midnightbsd.org/), [DragonflyBSD](https://www.dragonflybsd.org/), [GhostBSD](https://ghostbsd.org/), and [NomadBSD](https://nomadbsd.org/). I decided to take a look at a newer project: [helloSystem](https://github.com/helloSystem/hello). [For those who don't know, helloSystem is a project to recreate Mac OS on FreeBSD. It's still a work in progress, but is looking good. Below is a screenshot of the helloSystem desktop.]

![](/img/hello-screenshot.png "helloSystem desktop")

While exploring the [helloSystem wiki](https://hellosystem.github.io/docs/), I discovered a section entitled ["Applications"](https://hellosystem.github.io/docs/developer/applications.html). This page lists the requirements for an application to be written for helloSystem. The list of requirements includes:

* Simple to use
* Easy to hack on
* Open Source (2-clause BSD licensed by default, but similar permissive licenses may also be acceptable)
* Ideally written in PyQt

After reading this page, I thought of an article I read years ago, entitled ["Why you should be writing apps for Haiku"](https://web.archive.org/web/20161025140032/http://clasquin-johnson.co.za/michel/haiku/blog/2014/why-you-should-be-writing.html). [For reference, [Haiku](https://www.haiku-os.org/) is the open-source recreation of the BeOS operating system, which was created by Be Inc. Be was founded by a former Apple employee in the 1990s. Haiku is used and developed by a small, but devoted group of people.]

Here is a quote from the article. As you read it, replace "Haiku" with "helloSystem".

>So why should you be writing apps for Haiku? For one simple reason: you can be the first. Does Windows need another flat-file database? Does Mac OSX need another wall-paper utility? Does Linux need another text editor? Of course not. Those systems have been around for decades and  it is really, really hard to think of a new sort of app for which there aren't already a dozen well-established alternatives.
>
>Haiku needs EVERYTHING. Can you write a word processor? This is your chance to be a hero. Write something that will read and write the common document formats and do the basic word processing functions on Haiku, and everybody in the community will install it. You have the chance to be the first, to be the author of that program that all the other programmers have to compete against.

After reading that, I thought it might be fun to write some simple Mac-like programs for helloSystem. [From the looks of it, helloSystem needs a lot of applications.] To do that, I need to learn Python. Which shouldn't be too hard considering how many tutorials are out there.

Now that I have a goal, maybe I can finally learn to program. I will post about any app I create.



---

{{< endmatter >}}

{{< comment >}}
