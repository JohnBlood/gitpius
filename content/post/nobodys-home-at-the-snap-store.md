---
title: "Nobody's Home at the Snap Store"
date: "2022-05-03T22:36:51-04:00"
description: "My expierence with snaps over the last couple of years"
draft: true
tags:
 - "Snaps"
 - "Ubuntu"
categories:
 - "thoughts"
 - "programming"
# Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
meta:
  - date
  - categories
featured:
  url: /img/ldpl-logo.png # relative path of the image
  alt: LDPL mascot # alternate text for the image
  caption: LDPL mascot # image caption
  credit: Martín del Río # image credit
  previewOnly: false # show only preview image (true/false)
---

About 4 years ago, I created a [snap](https://snapcraft.io/dosage) to see if the hype was true. The snap ran without any issue for a while. However, recently the Snapcraft website has stopped working for me and I can't get any response for anyone. Is anyone at home?

<!--more-->

This saga begins in 2018. I'd been reading quite a bit about this new Linux software packaging system called snaps. This new system was "asgnostic" meaning that it would be available multiple distros. Just create a package once and have it available everywhere was the claim.


{{< comment >}}
