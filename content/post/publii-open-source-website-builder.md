---
title: "Publii — Open Source Website Builder"
date: "2017-11-18T22:02:40-04:00"
images: ["img/publii.png"]
description: "Publii is a great open source for creating websites."
tags: 
 - "open source"
categories: 
 - "apps"
 # Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post

---

![](/img/publii.png "Publii — Open Source Website Builder")

In this day and age, everyone either has a website or wants one. For those who want a website, there are many tools to choose from. Unfortunately, most of these options are not beginner-friendly. Here comes Publii to the rescue.

<!--more-->

[Publii](https://getpublii.com/) is a free and open source application for Window and Mac. (At the time of this writing, there is no Linux option.) It is available under the GPL v3. Publii allows you to easily create your site locally on your computer, instead of putting it immediately on the web. This can be helpful when you have a poor internet connection.

Publii is billed as an open-source CMS (Content Management System). Unlike most CMSes that you are probably familiar with, like WordPress or Joomla, Publii does not rely on a database to store information. Instead, the content of your website is stored in a series of static HTML pages. This is more secure because it means that hackers (or user error) cannot corrupt your website. Publii also makes sure that your site will load faster than a database-powered website by compressing HTML and CSS files and making use of Google's Accelerated Mobile Pages.

While most website building tools are very complex and have a steep learning curve, Publii is just the opposite. It is designed in such a way that even a newbie can get a website up and running in a matter of minutes. Publii has an intuitive post editor, that includes text styling, options to add images and tags, and post statistics.

The team behind Publii believe that your website should be easy to host, as well as, build. With that in mind, they gave Publii support for Amazon S3, GitHub Pages, Google Cloud, Netlify, and SFTP. Once you've uploaded your site to your hosting company of choice, we can synchronize any future changes with a single button click.

SEO (Search Engine Optimization) is important if you want your website to be discovered. However, it is usually one of the harder parts of building a website. Publii has built-in tools to make SEO a breeze. It also makes use of Open Graph, so your website can easily be shared on social media.

Publii is set up to handle more than one website. All you need to do is specify a site name and author, and you are good to go. Publii lets you color code each website, so you don't get them mixed up.

You can create your own custom styling for your website using Publii's built-in customization tools. You can also pick from 9 themes available on Publii's site. You can even create your own theme if you are familiar with Handlebars.

At the moment, Publii is in beta. Version 0.20.2 was released on November 10 and includes support for importing your site from WordPress. Publii is released under GPL v3. It is only available for Windows and Mac OS.

Give Publii a try and tell me what you think about it in the comments below. What is your favorite website creation tool?

---

{{< endmatter >}}

{{< comment >}}
