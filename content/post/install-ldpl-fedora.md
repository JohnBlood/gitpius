---
title: "How to Install the LDPL Programming Language on Fedora"
date: "2021-02-14T11:16:57-05:00"
# images: ["/img/ldpl-logo.png"]
description: "Fedora specific tricks to install the LDPL Programming Language"
tags: 
 - "Fedora"
 - "ldpl"
categories: 
 - "how-to"
 - "programming"
# Theme-Defined params
comments: true # Enable/disable Disqus comments for specific post
authorbox: true # Enable/disable Authorbox for specific post
toc: false # Enable/disable Table of Contents for specific post
mathjax: false # Enable/disable MathJax for specific post
related: true # Enable/disable Related content for specific post
meta:
  - date
  - categories
featured:
  url: /img/ldpl-logo.png # relative path of the image
  alt: LDPL mascot # alternate text for the image
  caption: LDPL mascot # image caption
  credit: Martín del Río # image credit
  previewOnly: false # show only preview image (true/false)

---

![](/img/ldpl-logo.png "Mascot for LPL lang")

Recently, I stumbled across an interesting little language named the [LDPL programming language](https://www.ldpl-lang.org/). It's inspired by COBOL and has several neat features. However, LDPL is not as popular or well known as other languages.

<!--more-->

>[LDPL](https://www.ldpl-lang.org/philosophy.html) (the Dinosaur Programming Language) is a language designed to be easy to understand and simple to write. Designed to be easy to learn and simple to use. We believe that coding should be easy and simple. Nothing should become a frustrating chore. Compiling source code should be easy and simple: a single, flagless command should be enough to compile any source code. Every statement in the language should do one and one thing only, not multiple things depending on the context. Complex, low level stuff like encoding, sockets, floating point number comparison, etc., should be dealt by the compiler, transparently to the user. Hard stuff should be made easier.

If you want to use LDPL, you need to follow the [installation instructions on GitHub](https://github.com/Lartu/ldpl#-how-to-install-ldpl). However, the instructions are pretty generic. Below are Fedora specific installation instructions.

First, you need to download the latest version of LDPL via git.

`git clone https://github.com/Lartu/ldpl.git`

Next, you need to use `cd ldpl` to enter the languages folder and run the `make` command.

In my case, I got the following error because LDPL needs to have C++ installed. I was under the impression that Fedora had C++ installed, but I was wrong.

```
[johnblood@itsfoss ldpl]$ make
cd src && g++ -Wall -std=gnu++11 -fpermissive -DVERSION='"4.5"'   -DVERSIONNAME='"Groovy Gualicho"'  -DCOMPILEDATE='"2021-02-07"' -DCOMPILEHOUR='"08:29:47"' -DLPMLOCATION='"~/ldpl/lpm/"' -DLDPLLIBLOCATION='"/usr/local/lib/ldpl"'  ldpl.cpp -o ldpl -static-libgcc -static-libstdc++
/usr/bin/ld: cannot find -lstdc++
collect2: error: ld returned 1 exit status
make: *** [makefile:59: all] Error 1
```
To install the required dependency, run `sudo dnf install libstdc++-static`.

Once the installation is complete, run `make` and then `sudo make install`.

You shouldn't encounter any other errors. Once the installation is complete, you can check to make it is working by running `ldpl -v` in the terminal. The output should look like this:

```
[johnblood@itsfoss ~]$ ldpl -v

 This is LDPL version 4.5 'Groovy Gualicho'.

 Copyright 2018-2020, Martín del Río (www.lartu.net).
 Built with amazing contributions from Chris West, Damián Garro,
 Ignacio Losiggio and other wonderful contributors.

 The LDPL Home Page can be found at www.ldpl-lang.org.
 The LDPL source code is available at www.github.com/lartu/ldpl.

 Complete documentation for LDPL should be found on this system
 using 'man ldpl'. If you have access to the internet, the
 documentation can also be found online at docs.ldpl-lang.org.

 LDPL may be copied only under the terms of the Apache License
 Version 2.0, which may be found in the LDPL repository.

 This binary was compiled on 2021-02-07 at 08:36:59.
```

### Update - 1-29-2022

I tried to install LDPL on a clean installation of Fedora 35 and ran into the following error:

```
[johnblood@fedora ldpl]$ make
cd src && g++ -Wall -std=gnu++11 -fpermissive -DVERSION='"4.5"'   -DVERSIONNAME='"Groovy Gualicho"'  -DCOMPILEDATE='"2022-01-29"' -DCOMPILEHOUR='"21:53:15"' -DLPMLOCATION='"~/ldpl/lpm/"' -DLDPLLIBLOCATION='"/usr/local/lib/ldpl"'  ldpl.cpp -o ldpl -static-libgcc -static-libstdc++
/bin/sh: line 1: g++: command not found
make: *** [makefile:59: all] Error 127
```
To fix this error, you need to install the `gcc-c++` package via `sudo dnf install gcc-c++`.

## Optional: Install Code Highlighting

LDPL ships with support for code highlighting for both Nano and Gedit/Pluma. To install, you need to navigate to either the `/highlighting/nano` or `/highlighting/gedit` folder.  When you are in the correct folder, run `./install.sh/`.

---

{{< endmatter >}}

{{< comment >}}
